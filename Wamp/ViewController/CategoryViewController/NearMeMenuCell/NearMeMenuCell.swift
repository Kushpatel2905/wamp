//
//  NearMeMenuCell.swift
//  Wamp
//
//  Created by Kushal Patel on 8/14/17.
//  Copyright © 2017 Kushal Patel. All rights reserved.
//

import UIKit

class NearMeMenuCell: UICollectionViewCell {
    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var imgIcon : UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.imgIcon.layer.cornerRadius = self.imgIcon.frame.size.width / 2
        self.imgIcon.clipsToBounds = true

        // Initialization code
    }

}
