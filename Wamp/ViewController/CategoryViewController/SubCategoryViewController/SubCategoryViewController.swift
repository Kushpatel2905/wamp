//
//  SubCategoryViewController.swift
//  Wamp
//
//  Created by Rameshbhai Patel on 16/11/17.
//  Copyright © 2017 Kushal Patel. All rights reserved.
//

import UIKit
import SDWebImage
import CoreLocation
import MapKit
class SubCategoryViewController: UIViewController,UICollectionViewDelegate,CLLocationManagerDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    fileprivate var Businessplaces = [Place]()
    @IBOutlet var collView : UICollectionView?
    @IBOutlet var bottomViewX:NSLayoutConstraint?
    var arrCategory : NSMutableArray! = NSMutableArray.init()
    var objSelectedCategory : Category!
    var arViewController: ARViewController!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = objSelectedCategory.catResturantName
        
        self.collView?.register(UINib.init(nibName: "NearMeMenuCell", bundle: nil), forCellWithReuseIdentifier:"NearMeMenu")
        
        /*let settingsButton = UIBarButtonItem(title: NSString(string: "\u{2699}\u{0000FE0E}") as String, style: .plain, target: self, action: #selector(CategoryViewController.goToSettingview))
        let font = UIFont.systemFont(ofSize: 28) // adjust the size as required
        let attributes = [NSFontAttributeName : font]
        settingsButton.setTitleTextAttributes(attributes, for: .normal)
        
        self.navigationItem.rightBarButtonItem = settingsButton
        */
        
        /*let objDataMange = DataManager.init()
        SwiftLoader.show(animated: true)
        objDataMange.getSubCategoryList(strCatId: self.objSelectedCategory.catID) { (isSuccess, arrCat) in
            DispatchQueue.main.sync {
                self.arrCategory = NSMutableArray.init(array: arrCat!)
                SwiftLoader.hide()
                self.collView?.reloadData()
            }
        }*/
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.view.addSubview(appDelegate.addBottomView())
        self.bottomViewX?.constant = 70
        let view = self.navigationController?.view.viewWithTag(1001)
        view?.isHidden = false
    }
    func goToSettingview() -> Void {
        
        let placeList = SettingViewController.init()
        self.navigationController?.pushViewController(placeList, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrCategory.count;
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return  CGSize.init(width: (UIScreen.main.bounds.size.width-30)/2, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NearMeMenu", for: indexPath) as! NearMeMenuCell
        
        let objCat = self.arrCategory.object(at: indexPath.row) as! SubCategory
        cell.lblTitle.text = objCat.subCatName
        
        cell.imgIcon.sd_setImage(with: URL(string: objCat.subCatImageURL), placeholderImage: UIImage(named: "placeholder.png"))
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        let objDataMange = DataManager.init()
        SwiftLoader.show(animated: true)
        let objCat = self.arrCategory.object(at: indexPath.row) as! SubCategory
        objDataMange.getBusinessList(strSubCatId: objCat.subCatID) { (issuccess, arr) in
            DispatchQueue.main.sync {
                SwiftLoader.hide()
                let arrBusiness : NSMutableArray! = NSMutableArray.init(array: arr!)
                for i in (0..<(arrBusiness as AnyObject).count)
                {
                    let objBusiness = arrBusiness.object(at: i) as! BusinessList
                    let latitude = Double(objBusiness.businessLatitude)!
                    let longitude = Double(objBusiness.businessLongitude)!
                    let location = CLLocation(latitude: latitude, longitude: longitude)
                    let place = Place(location: location, reference: objBusiness.businessFirstname, name: objBusiness.businessBusinessName, address: objBusiness.businessAddress)
                    self.Businessplaces.append(place)
                }
                if arrBusiness.count > 0{
                    self.arViewController = ARViewController()
                    self.arViewController.dataSource = self
                    self.arViewController.maxDistance = 0
                    self.arViewController.maxVisibleAnnotations = 30
                    self.arViewController.maxVerticalLevel = 5
                    self.arViewController.headingSmoothingFactor = 0.05
                    self.arViewController.trackingManager.userDistanceFilter = 25
                    self.arViewController.trackingManager.reloadDistanceFilter = 75
                    self.arViewController.setAnnotations(self.Businessplaces)
                    self.arViewController.uiOptions.debugEnabled = false
                    self.arViewController.uiOptions.closeButtonEnabled = true
                    self.arViewController.navigationController?.isNavigationBarHidden = true

                    self.arViewController.isSubCategory = true
                    self.arViewController.arrBusiness = arrBusiness
                    let navController = UINavigationController(rootViewController: self.arViewController)
                    self.present(navController, animated: true, completion: nil)
                }
            }
        }
    }
    func showInfoView(forPlace place: Place) {
        let alert = UIAlertController(title: place.placeName , message: place.infoText, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        arViewController.present(alert, animated: true, completion: nil)
    }
}

    extension SubCategoryViewController: ARDataSource {
        func ar(_ arViewController: ARViewController, viewForAnnotation: ARAnnotation) -> ARAnnotationView {
            let annotationView = AnnotationView()
            annotationView.annotation = viewForAnnotation
            annotationView.delegate = self
            annotationView.frame = CGRect(x: 0, y: 0, width: 150, height: 50)
            
            return annotationView
        }
    }
    
    extension SubCategoryViewController: AnnotationViewDelegate {
        func didTouch(annotationView: AnnotationView) {
            if let annotation = annotationView.annotation as? Place {
                let placesLoader = PlacesLoader()
                placesLoader.loadDetailInformation(forPlace: annotation) { resultDict, error in
                    
                    if let infoDict = resultDict?.object(forKey: "result") as? NSDictionary {
                        annotation.phoneNumber = infoDict.object(forKey: "formatted_phone_number") as? String
                        annotation.website = infoDict.object(forKey: "website") as? String
                        
                        self.showInfoView(forPlace: annotation)
                    }
                }
                
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

