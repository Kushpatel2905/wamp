//
//  CategoryViewController.swift
//  Wamp
//
//  Created by Rameshbhai Patel on 16/11/17.
//  Copyright © 2017 Kushal Patel. All rights reserved.
//

import UIKit
import SDWebImage
import CoreLocation
import MapKit

class CategoryViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,showSubcategoriesWiseItemListProtocol {
    @IBOutlet var tblCategory : UITableView?
    @IBOutlet var bottomViewX:NSLayoutConstraint?
    @IBOutlet var topConstraint:NSLayoutConstraint?
    var arrCategory : NSMutableArray! = NSMutableArray.init()

    // Call for augmented reality object
    fileprivate var Businessplaces = [Place]()
    var arViewController: ARViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let imageView = UIImageView(image: UIImage(named: "header_cat_bg"))
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        let titleView = UIView(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
        imageView.frame = titleView.bounds
        titleView.addSubview(imageView)
        
        self.navigationItem.titleView = titleView

        tblCategory?.register(UINib.init(nibName: "categoryCell", bundle: nil), forCellReuseIdentifier: "categoryCell")
        tblCategory?.register(UINib.init(nibName: "SearchCell", bundle: nil), forCellReuseIdentifier:"SearchCell")
        tblCategory?.rowHeight = UITableViewAutomaticDimension
        tblCategory?.estimatedRowHeight = 135

        
        let settingsButton = UIBarButtonItem(title: NSString(string: "\u{2699}\u{0000FE0E}") as String, style: .plain, target: self, action: #selector(CategoryViewController.goToSettingview))
        let font = UIFont.systemFont(ofSize: 28) // adjust the size as required
        let attributes = [NSFontAttributeName : font]
        settingsButton.setTitleTextAttributes(attributes, for: .normal)
        
        self.navigationItem.rightBarButtonItem = settingsButton
        
        let objDataMange = DataManager.init()
        SwiftLoader.show(animated: true)
        objDataMange.getCategoryList { (isSuccess, arrCat) in
            DispatchQueue.main.sync {
                self.arrCategory = NSMutableArray.init(array: arrCat!)
                SwiftLoader.hide()
                self.tblCategory?.reloadData()
            }
        }
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.view.addSubview(appDelegate.addBottomView())
        self.bottomViewX?.constant = 70
        let view = self.navigationController?.view.viewWithTag(1001)
        view?.isHidden = false
    }
    func goToSettingview() -> Void {
        let placeList = SettingViewController.init()
        self.navigationController?.pushViewController(placeList, animated: true)
    }
    
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 50;
        }
        return 160;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrCategory.count+1;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell : SearchCell = tableView.dequeueReusableCell(withIdentifier: "SearchCell") as! SearchCell
            
            return cell;
        }
        
        let cell : categoryCell = tableView.dequeueReusableCell(withIdentifier: "categoryCell") as! categoryCell
        cell.delegate = self;

        let objCat = self.arrCategory.object(at: indexPath.row-1) as! Category
        cell.lblCatName.text = objCat.catResturantName
        cell.imgBg.sd_setImage(with: URL(string: objCat.catImageURL), placeholderImage: UIImage.init())
        cell.imgBg.backgroundColor = UIColor.init(red: 219.0/255.0, green: 219.0/255.0, blue: 219.0/255.0, alpha: 1.0)
        cell.arrSubCategory = objCat.arrSubCategory;
        return cell
    }
    
    // Protocol call while trigger Subcategory item
    func showSubCategoryItems(objSubCat: SubCategory) {
        let objDataMange = DataManager.init()
        SwiftLoader.show(animated: true)
        objDataMange.getBusinessList(strSubCatId: objSubCat.subCatID) { (issuccess, arr) in
            DispatchQueue.main.sync {
                SwiftLoader.hide()
                let arrBusiness : NSMutableArray! = NSMutableArray.init(array: arr!)
                for i in (0..<(arrBusiness as AnyObject).count)
                {
                    let objBusiness = arrBusiness.object(at: i) as! BusinessList
                    let latitude = Double(objBusiness.businessLatitude)!
                    let longitude = Double(objBusiness.businessLongitude)!
                    let location = CLLocation(latitude: latitude, longitude: longitude)
                    let place = Place(location: location, reference: objBusiness.businessFirstname, name: objBusiness.businessBusinessName, address: objBusiness.businessAddress)
                    self.Businessplaces.append(place)
                }
                if arrBusiness.count > 0{
                    
                    let placeList = PlaceListViewController.init()
                    placeList.arrPlace = arrBusiness
                    placeList.arrARView = self.Businessplaces
                    self.navigationController?.pushViewController(placeList, animated: true)
                    
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func showInfoView(forPlace place: Place) {
        let alert = UIAlertController(title: place.placeName , message: place.infoText, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        arViewController.present(alert, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CategoryViewController: ARDataSource {
    func ar(_ arViewController: ARViewController, viewForAnnotation: ARAnnotation) -> ARAnnotationView {
        let annotationView = AnnotationView()
        annotationView.annotation = viewForAnnotation
        annotationView.delegate = self
        annotationView.frame = CGRect(x: 0, y: 0, width: 150, height: 50)
        
        return annotationView
    }
}

extension CategoryViewController: AnnotationViewDelegate {
    func didTouch(annotationView: AnnotationView) {
        if let annotation = annotationView.annotation as? Place {
            let placesLoader = PlacesLoader()
            placesLoader.loadDetailInformation(forPlace: annotation) { resultDict, error in
                
                if let infoDict = resultDict?.object(forKey: "result") as? NSDictionary {
                    annotation.phoneNumber = infoDict.object(forKey: "formatted_phone_number") as? String
                    annotation.website = infoDict.object(forKey: "website") as? String
                    
                    self.showInfoView(forPlace: annotation)
                }
            }
            
        }
    }
}
