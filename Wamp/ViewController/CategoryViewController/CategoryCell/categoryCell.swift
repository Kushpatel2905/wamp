//
//  categoryCell.swift
//  Wamp
//
//  Created by Rameshbhai Patel on 05/01/18.
//  Copyright © 2018 Kushal Patel. All rights reserved.
//

import UIKit

// For open Augmented
protocol showSubcategoriesWiseItemListProtocol : NSObjectProtocol {
    func showSubCategoryItems(objSubCat: SubCategory) -> Void;
}

class categoryCell: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    weak var delegate: showSubcategoriesWiseItemListProtocol?

    @IBOutlet var imgBg : UIImageView!
    @IBOutlet var lblCatName : UILabel!
    @IBOutlet var collSubCat : UICollectionView!
    var arrSubCategory = NSMutableArray()
    override func awakeFromNib() {
        super.awakeFromNib()
        self.collSubCat?.register(UINib.init(nibName: "NearMeMenuCell", bundle: nil), forCellWithReuseIdentifier:"NearMeMenu")
//        self.collSubCat.reloadData()
        // Initialization code
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrSubCategory.count;
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return  CGSize.init(width: 70, height: 80)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NearMeMenu", for: indexPath) as! NearMeMenuCell
        
        let objCat = self.arrSubCategory.object(at: indexPath.row) as! SubCategory
        cell.lblTitle.text = objCat.subCatName.capitalized
        
        cell.imgIcon.sd_setImage(with: URL(string: objCat.subCatImageURL), placeholderImage: UIImage(named: ""))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let objCat = self.arrSubCategory.object(at: indexPath.row) as! SubCategory
        if((delegate?.responds(to: Selector(("showSubCategoryItems:")))) != nil)
        {
            delegate?.showSubCategoryItems(objSubCat: objCat);
        }        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension categoryCell {
    
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
        
        collSubCat.delegate = dataSourceDelegate
        collSubCat.dataSource = dataSourceDelegate
        collSubCat.tag = row
        collSubCat.setContentOffset(collSubCat.contentOffset, animated:false) // Stops collection view if it was scrolling.
        collSubCat.reloadData()
    }
    
    var collectionViewOffset: CGFloat {
        set { collSubCat.contentOffset.x = newValue }
        get { return collSubCat.contentOffset.x }
    }
}

