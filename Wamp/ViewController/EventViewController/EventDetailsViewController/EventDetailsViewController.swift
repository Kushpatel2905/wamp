//
//  EventDetailsViewController.swift
//  Wamp
//
//  Created by Rameshbhai Patel on 10/01/18.
//  Copyright © 2018 Kushal Patel. All rights reserved.
//

import UIKit

class EventDetailsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet var tblEventDetail : UITableView?
    @IBOutlet var btnGoing : UIButton?
    @IBOutlet var bottomViewX:NSLayoutConstraint?
    
    public var objEvent : Event = Event()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = objEvent.eventName
        
        tblEventDetail?.register(UINib.init(nibName: "EventCategoryCell", bundle: nil), forCellReuseIdentifier: "EventCategoryCell")
        tblEventDetail?.register(UINib.init(nibName: "EventDetailsCell", bundle: nil), forCellReuseIdentifier: "EventDetailsCell")
        tblEventDetail?.rowHeight = UITableViewAutomaticDimension
        tblEventDetail?.estimatedRowHeight = 106
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.view.addSubview(appDelegate.addBottomView())
        self.bottomViewX?.constant = 70
        let view = self.navigationController?.view.viewWithTag(1001)
        view?.isHidden = false
    }
    @IBAction func btnGoingAction(_ sender: UIButton) {
        print("Test")
        let objDataMange = DataManager.init()
        SwiftLoader.show(animated: true)
        objDataMange.goingToButtonAction(strEventId: objEvent.eventID) { (isSuccess, arrCat) in
            DispatchQueue.main.sync {
                SwiftLoader.hide()
                let alert = UIAlertController(title: "Alert", message: (arrCat as Any as! String), preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return (UIScreen.main.bounds.height - 30)/2;
        }
        return UITableViewAutomaticDimension;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell : EventCategoryCell = tableView.dequeueReusableCell(withIdentifier: "EventCategoryCell") as! EventCategoryCell
            cell.lblTitle.isHidden = true;
//            cell.imgHeightConstraint.constant = 120
           // cell.imgIcon.contentMode = .scaleAspectFit
            cell.imgIcon.sd_setImage(with: URL(string: objEvent.eventBanner), placeholderImage: UIImage(named: "header1bg"))
            return cell
        }
        let cell : EventDetailsCell = tableView.dequeueReusableCell(withIdentifier: "EventDetailsCell") as! EventDetailsCell
        if indexPath.row == 1{
            cell.lblTitle?.text = "Event Name";
            cell.lblMsg?.text = objEvent.eventName;
        }else if indexPath.row == 2{
            cell.lblTitle?.text = "Date";
            cell.lblMsg?.text = objEvent.eventDate;
        }else if indexPath.row == 3{
            cell.lblTitle?.text = "Location";
            cell.lblMsg?.text = objEvent.eventLocation;
        }else if indexPath.row == 4{
            cell.lblTitle?.text = "Category";
            cell.lblMsg?.text = objEvent.eventCategoryId;
        }else if indexPath.row == 5{
            cell.lblTitle?.text = "Type";
            cell.lblMsg?.text = objEvent.eventType;
        }else if indexPath.row == 6{
            cell.lblTitle?.text = "Crowd";
            cell.lblMsg?.text = objEvent.eventCrowdId;
        }else{
            cell.lblTitle?.text = "Time";
            cell.lblMsg?.text = objEvent.eventTime;
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
