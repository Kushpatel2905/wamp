//
//  EventCategoryCell.swift
//  Wamp
//
//  Created by Rameshbhai Patel on 10/01/18.
//  Copyright © 2018 Kushal Patel. All rights reserved.
//

import UIKit

class EventCategoryCell: UITableViewCell {
    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var imgIcon : UIImageView!
    @IBOutlet weak var imgHeightConstraint: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
