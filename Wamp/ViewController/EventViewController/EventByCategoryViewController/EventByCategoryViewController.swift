//
//  EventByCategoryViewController.swift
//  Wamp
//
//  Created by Rameshbhai Patel on 10/01/18.
//  Copyright © 2018 Kushal Patel. All rights reserved.
//

import UIKit

class EventByCategoryViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet var lblNoRecord : UILabel?
    @IBOutlet var tblEventCategory : UITableView?
    @IBOutlet var bottomViewX:NSLayoutConstraint?
    
    var arrEventCat : NSMutableArray = NSMutableArray() // arrEventCat List

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Event"
        tblEventCategory?.register(UINib.init(nibName: "EventCategoryCell", bundle: nil), forCellReuseIdentifier: "EventCategoryCell")
        tblEventCategory?.rowHeight = UITableViewAutomaticDimension
        tblEventCategory?.estimatedRowHeight = 120

        
        // Show Right Navigationbar Button
        let settingsButton = UIBarButtonItem(title: NSString(string: "\u{2699}\u{0000FE0E}") as String, style: .plain, target: self, action: #selector(EventViewController.goToSettingview))
        let font = UIFont.systemFont(ofSize: 28) // adjust the size as required
        let attributes = [NSFontAttributeName : font]
        settingsButton.setTitleTextAttributes(attributes, for: .normal)
        self.navigationItem.rightBarButtonItem = settingsButton
        
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.view.addSubview(appDelegate.addBottomView())

        self.callWebservicesForEventCategoryList() // call webservices for getting all event cateogry list
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.view.addSubview(appDelegate.addBottomView())
        self.bottomViewX?.constant = 70
        let view = self.navigationController?.view.viewWithTag(1001)
        view?.isHidden = false
    }
    func callWebservicesForEventCategoryList() {
        let objDataMange = DataManager.init()
        SwiftLoader.show(animated: true)
        objDataMange.getEventCategoryList { (isSuccess, arrCat) in
            DispatchQueue.main.sync {
                self.arrEventCat = NSMutableArray.init(array: arrCat!)
                self.lblNoRecord?.isHidden = true
                if self.arrEventCat.count == 0{
                    self.lblNoRecord?.isHidden = false
                }
                SwiftLoader.hide()
                self.tblEventCategory?.reloadData()
            }
        }
    }
    
    // open Setting ViewController
    func goToSettingview() -> Void {
        
        let placeList = SettingViewController.init()
        self.navigationController?.pushViewController(placeList, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrEventCat.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : EventCategoryCell = tableView.dequeueReusableCell(withIdentifier: "EventCategoryCell") as! EventCategoryCell
        let objEventCat = self.arrEventCat.object(at: indexPath.row) as! EventCategory
        cell.lblTitle.text = objEventCat.eventCatName
        cell.imgHeightConstraint.constant = (UIScreen.main.bounds.size.height - 30) / 3
        cell.imgIcon.sd_setImage(with: URL(string: objEventCat.eventCatURL), placeholderImage: UIImage(named: ""))
        cell.imgIcon.backgroundColor = UIColor.init(red: 219.0/255.0, green: 219.0/255.0, blue: 219.0/255.0, alpha: 1.0)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let objEventView = EventViewController.init()
        objEventView.objEventCat = self.arrEventCat.object(at: indexPath.row) as! EventCategory
        self.navigationController?.pushViewController(objEventView, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
