//
//  EventViewController.swift
//  Wamp
//
//  Created by Rameshbhai Patel on 07/01/18.
//  Copyright © 2018 Kushal Patel. All rights reserved.
//

import UIKit

class EventViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,EventCellDelegate {
    @IBOutlet var lblNoRecord : UILabel?
    @IBOutlet var tblEvent : UITableView?
    @IBOutlet var bottomViewX:NSLayoutConstraint?
    public var objEventCat : EventCategory = EventCategory()
    var arrEvent : NSMutableArray = NSMutableArray() // arrEvent List

    override func viewDidLoad() {
        super.viewDidLoad()
        print(objEventCat.eventCatID)
        self.title = "Event"
        
        tblEvent?.register(UINib.init(nibName: "EventCell", bundle: nil), forCellReuseIdentifier: "EventCell")
        tblEvent?.rowHeight = UITableViewAutomaticDimension
        tblEvent?.estimatedRowHeight = 106

        // Show Right Navigationbar Button
        let settingsButton = UIBarButtonItem(title: NSString(string: "\u{2699}\u{0000FE0E}") as String, style: .plain, target: self, action: #selector(EventViewController.goToSettingview))
        let font = UIFont.systemFont(ofSize: 28) // adjust the size as required
        let attributes = [NSFontAttributeName : font]
        settingsButton.setTitleTextAttributes(attributes, for: .normal)
        self.navigationItem.rightBarButtonItem = settingsButton
        
        self.callWebservicesForEventList() // call webservices for getting all event list
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.view.addSubview(appDelegate.addBottomView())
        self.bottomViewX?.constant = 70
        let view = self.navigationController?.view.viewWithTag(1001)
        view?.isHidden = false
    }
    // call webservices for event list
    func callWebservicesForEventList() {
        let objDataMange = DataManager.init()
        SwiftLoader.show(animated: true)
        objDataMange.getEventList(strEventCatId: self.objEventCat.eventCatID) { (isSuccess, arrCat) in
            DispatchQueue.main.sync {
                self.arrEvent = NSMutableArray.init(array: arrCat!)
                self.lblNoRecord?.isHidden = true
                if self.arrEvent.count == 0{
                    self.lblNoRecord?.isHidden = false
                }
                SwiftLoader.hide()
                self.tblEvent?.reloadData()
            }
        }
    }
    
    // open Setting ViewController
    func goToSettingview() -> Void {
        
        let placeList = SettingViewController.init()
        self.navigationController?.pushViewController(placeList, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrEvent.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : EventCell = tableView.dequeueReusableCell(withIdentifier: "EventCell") as! EventCell
        cell.delegate = self
        cell.indexPath = indexPath
        let objEventCat = self.arrEvent.object(at: indexPath.row) as! Event
        cell.lblTitle?.text = objEventCat.eventName//.uppercased()
        cell.lblDescription?.text = objEventCat.eventDescription//.uppercased()
        cell.lblDate?.text = objEventCat.eventDate
        cell.imgPicture?.sd_setImage(with: URL(string: objEventCat.eventBanner), placeholderImage: UIImage(named: ""))
        cell.imgPicture?.backgroundColor = UIColor.init(red: 219.0/255.0, green: 219.0/255.0, blue: 219.0/255.0, alpha: 1.0)
        return cell
    }
    
    func buttonEventDetailsTapped(at index: IndexPath) {
        let objEventDetailsView = EventDetailsViewController.init()
        objEventDetailsView.objEvent = self.arrEvent.object(at: index.row) as! Event
        self.navigationController?.pushViewController(objEventDetailsView, animated: true)
        print("button tapped at index:\(index)")
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let objEventDetailsView = EventDetailsViewController.init()
        objEventDetailsView.objEvent = self.arrEvent.object(at: indexPath.row) as! Event
        self.navigationController?.pushViewController(objEventDetailsView, animated: true)
        print("button tapped at index:\(index)")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

