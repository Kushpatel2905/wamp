//
//  EventCell.swift
//  Wamp
//
//  Created by Rameshbhai Patel on 10/01/18.
//  Copyright © 2018 Kushal Patel. All rights reserved.
//

import UIKit
protocol EventCellDelegate{
    func buttonEventDetailsTapped(at index:IndexPath)
}

class EventCell: UITableViewCell {
    var delegate:EventCellDelegate!
    var indexPath:IndexPath!

    @IBOutlet var imgPicture : UIImageView?
    @IBOutlet var lblTitle : UILabel?
    @IBOutlet var lblDescription : UILabel?
    @IBOutlet var lblDate : UILabel?
    @IBOutlet var btnDetails : UIButton?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func buttonEventDetailsAction(_ sender: UIButton) {
        self.delegate?.buttonEventDetailsTapped(at: indexPath)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
