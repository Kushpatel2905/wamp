//
//  CreateAccountViewController.swift
//  Wamp
//
//  Created by Rameshbhai Patel on 11/11/17.
//  Copyright © 2017 Kushal Patel. All rights reserved.
//

import UIKit

class CreateAccountViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet var txtName : UITextField!
    @IBOutlet var txtEmail : UITextField!
    @IBOutlet var txtPassword : UITextField!
    @IBOutlet var txtConfirmPsw : UITextField!
    
    @IBOutlet var btnSubmit : UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtName.delegate = self
        txtEmail.delegate = self
        txtPassword.delegate = self
        txtConfirmPsw.delegate = self
        
        self.navigationController?.view.addSubview(appDelegate.addBottomView())
        let imageView = UIImageView(image: UIImage(named: "headerbg"))
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        let titleView = UIView(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
        imageView.frame = titleView.bounds
        titleView.addSubview(imageView)
        
        self.navigationItem.titleView = titleView
        self.navigationController?.isNavigationBarHidden = false
        
        // Do any additional setup after loading the view.
    }
    /* Submit Button Action */
    @IBAction func btnSubmitTapped () -> Void{
        let objUtility = Utility()
        if txtName.text == "" {
            objUtility.showAlertMessage (vc: self, title: "Register", message: "Please enter UserName", actionTitles: ["OK"], actions: [{action1 in
                }, nil])
            return
        }else if txtEmail.text == "" {
            objUtility.showAlertMessage (vc: self, title: "Register", message: "Please enter Email", actionTitles: ["OK"], actions: [{action1 in
                }, nil])
            return
        }else if !objUtility.isValidEmail(testStr: (txtEmail?.text)!) {
            objUtility.showAlertMessage (vc: self, title: "Register", message: "Please enter Valid Email", actionTitles: ["OK"], actions: [{action1 in
                }, nil])
            return
        }else if txtPassword.text == "" {
            objUtility.showAlertMessage (vc: self, title: "Register", message: "Please enter Password", actionTitles: ["OK"], actions: [{action1 in
                }, nil])
            return
        }else if txtConfirmPsw.text != txtPassword.text{
            objUtility.showAlertMessage (vc: self, title: "Register", message: "Both password not match", actionTitles: ["OK"], actions: [{action1 in
                }, nil])
            return
        }else{
            SwiftLoader.show(animated: true)
            let objDatamange = DataManager.init();
            
            let dic : NSMutableDictionary = NSMutableDictionary()
            dic.setValue(txtName.text, forKey: "Name")
            dic.setValue(txtName.text, forKey: "UserName")
            dic.setValue(txtEmail.text, forKey: "Email")
            dic.setValue(txtPassword.text, forKey: "Password")
            objDatamange.doRegister(dictRegister: dic) { (isSuccess, dictResponse) in
                DispatchQueue.main.sync {
                    let dict = dictResponse as NSDictionary!
                    if ((dict?.value(forKey: "data")) != nil){
                        User.sharedInstance.userToken = dictResponse?.value(forKey: "auth_key") as! String!
                        let objHome : HomeViewController = HomeViewController.init()
                        self.navigationController?.pushViewController(objHome, animated: true)
                    }
                    SwiftLoader.hide()
                }
                
            }
            
            objUtility.showAlertMessage (vc: self, title: "Register", message: "Submit Successfully!!!!", actionTitles: ["OK"], actions: [{action1 in
                }, nil])
            return
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder()
        if txtName == textField {
            txtName.resignFirstResponder()
            txtEmail.becomeFirstResponder()
        }else if txtEmail == textField{
            txtEmail.resignFirstResponder()
            txtPassword.becomeFirstResponder()
        }else if txtPassword == textField{
            txtPassword.resignFirstResponder()
            txtConfirmPsw.becomeFirstResponder()
        }else if txtConfirmPsw == textField{
            txtConfirmPsw.resignFirstResponder()
        }
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
