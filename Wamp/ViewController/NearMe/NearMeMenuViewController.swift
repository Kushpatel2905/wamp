//
//  NearMeMenuViewController.swift
//  Wamp
//
//  Created by Kushal Patel on 8/14/17.
//  Copyright © 2017 Kushal Patel. All rights reserved.
//

import UIKit

class NearMeMenuViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    @IBOutlet var collView : UICollectionView?
    @IBOutlet var bottomViewX:NSLayoutConstraint?
    var arrCategory : NSMutableArray! = NSMutableArray.init()
    override func viewDidLoad() {
        super.viewDidLoad()
   
        let imageView = UIImageView(image: UIImage(named: "headerbg"))
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        let titleView = UIView(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
        imageView.frame = titleView.bounds
        titleView.addSubview(imageView)
        self.navigationItem.titleView = titleView
        self.collView?.register(UINib.init(nibName: "NearMeMenuCell", bundle: nil), forCellWithReuseIdentifier:"NearMeMenu")
       
        let settingsButton = UIBarButtonItem(title: NSString(string: "\u{2699}\u{0000FE0E}") as String, style: .plain, target: self, action: #selector(NearMeMenuViewController.goToSettingview))
        let font = UIFont.systemFont(ofSize: 28) // adjust the size as required
        let attributes = [NSFontAttributeName : font]
        settingsButton.setTitleTextAttributes(attributes, for: .normal)
        
        self.navigationItem.rightBarButtonItem = settingsButton
        
        let objDataMange = DataManager.init()
        SwiftLoader.show(animated: true)
        objDataMange.getCategoryList { (isSuccess, arrCat) in
            
            DispatchQueue.main.sync {
                self.arrCategory = NSMutableArray.init(array: arrCat!)
                SwiftLoader.hide()
                self.collView?.reloadData()
            }
        }
        
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.view.addSubview(appDelegate.addBottomView())
        self.bottomViewX?.constant = 70
        let view = self.navigationController?.view.viewWithTag(1001)
        view?.isHidden = false
    }
    func goToSettingview() -> Void {
        
        let placeList = SettingViewController.init()
        self.navigationController?.pushViewController(placeList, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrCategory.count;
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return  CGSize.init(width: (UIScreen.main.bounds.size.width-30)/2, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NearMeMenu", for: indexPath) as! NearMeMenuCell
        
        let objCat = self.arrCategory.object(at: indexPath.row) as! Category
        cell.lblTitle.text = objCat.catResturantName
        
//        cell.imgIcon.sd_setImage(with: URL(string: objCat.catImageURL), placeholderImage: UIImage(named: "placeholder.png"))

        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
//        let placeList = CategoryDetailsViewController.init()
//        placeList.objSelectedCategory = self.arrCategory.object(at: indexPath.row) as! Category
//        self.navigationController?.pushViewController(placeList, animated: true)
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
