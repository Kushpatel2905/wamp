//
//  PlaceDetailsWebViewController.swift
//  Wamp
//
//  Created by Rameshbhai Patel on 19/11/17.
//  Copyright © 2017 Kushal Patel. All rights reserved.
//

import UIKit

class PlaceDetailsWebViewController: UIViewController,UIWebViewDelegate {
    var objBusiness : BusinessList!

    @IBOutlet var webPlace : UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // loading URL :
        webPlace.delegate = self
        let urlString = self.objBusiness.businessWebSite
        
        if let url = URL(string: (urlString?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)!)   {
            let request = URLRequest(url: url as URL)
            webPlace.loadRequest(request)
        }
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.view.addSubview(appDelegate.addBottomView())
        let view = self.navigationController?.view.viewWithTag(1001)
        view?.isHidden = false
    }
    func webViewDidStartLoad(_ webView: UIWebView) {
     SwiftLoader.show(animated: true)
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error)
    {
     SwiftLoader.hide()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        SwiftLoader.hide()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
