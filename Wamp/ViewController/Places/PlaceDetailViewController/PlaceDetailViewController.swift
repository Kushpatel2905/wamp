//
//  PlaceDetailViewController.swift
//  Wamp
//
//  Created by Kushal Patel on 8/16/17.
//  Copyright © 2017 Kushal Patel. All rights reserved.
//

import UIKit
import MapKit

@available(iOS 10.0, *)
class PlaceDetailViewController: UIViewController,MKMapViewDelegate,CLLocationManagerDelegate,UITableViewDelegate,UITableViewDataSource {
    
    var objBusiness : BusinessList!
    var isReview = false
    
    @IBOutlet var bottomViewX:NSLayoutConstraint?
    @IBOutlet var mapView : MKMapView?
    @IBOutlet var segDetails : UISegmentedControl?
    @IBOutlet var tblDetails : UITableView?
    @IBOutlet var btnCall : UIButton?
    @IBOutlet var btnWebsite : UIButton?
    @IBOutlet var btnDistance : UIButton?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = objBusiness.businessBusinessName
        self.navigationController?.isNavigationBarHidden = false
        self.mapView?.isRotateEnabled = false
        self.mapView?.delegate = self
        self.mapView?.showsUserLocation = true
        
        btnDistance?.setTitle(String(self.objBusiness.businessDistance), for: .normal)
        
        let lat = (self.objBusiness.businessLatitude as NSString).doubleValue
        let long = (self.objBusiness.businessLongitude as NSString).doubleValue

        var annotations = [MKPointAnnotation]()
        let coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
        
        let span = MKCoordinateSpanMake(0.0275, 0.0275)
        let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: lat, longitude: long), span: span)
        mapView?.setRegion(region, animated: true)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinate
        annotation.title = (self.objBusiness.businessBusinessName)!
        annotations.append(annotation)
        self.mapView?.addAnnotations(annotations)

        tblDetails?.register(UINib.init(nibName: "PlaceDetailsCell", bundle: nil), forCellReuseIdentifier: "DetailsCell")
        
        tblDetails?.rowHeight = UITableViewAutomaticDimension
        tblDetails?.estimatedRowHeight = 90
        tblDetails?.tableFooterView = UIView.init()

        // Make second segment selected
        segDetails?.selectedSegmentIndex = 0
        
        //Add function to handle Value Changed events
        segDetails?.addTarget(self, action: #selector(PlaceDetailViewController.segmentedValueChanged(_:)), for: .valueChanged)
        SwiftLoader.show(animated: true)
        DataManager.init().getEstablishmentDetails(strSubCatId: self.objBusiness.businessID) { (isSuccess, dictDetails) in
            
            DispatchQueue.main.sync {
                SwiftLoader.hide()
                self.objBusiness.arrComments =  NSMutableArray.init(array: dictDetails?.value(forKey: "comments") as! NSArray)
                
            }
        }

        // Do any additional setup after loading the view.
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        print("viewForAnnotation \(String(describing: annotation.title))")
        if annotation is MKUserLocation {
            return nil
        }
        let reuseID = "pin"
        var pinView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseID) as? MKPinAnnotationView
        if(pinView == nil) {
            pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: reuseID)
            pinView!.canShowCallout = true
            pinView!.animatesDrop = true
        }
        return pinView
    }
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        print("regionDidChangeAnimated")
//        let annotations = self.mapView?.annotations
//        self.mapView?.removeAnnotations(annotations!)
//        self.mapView?.addAnnotations(annotations!)
    }
    /* Call Button Action */
    @IBAction func btnCallTapped () -> Void{
        self.callNumber(phoneNumber: self.objBusiness.businessContact)
    }
    private func callNumber(phoneNumber:String) {
        let objUtility = Utility()
        if let url = URL(string: "tel://\(phoneNumber)"), UIApplication.shared.canOpenURL(url) {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(url)) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }else{
                objUtility.showAlertMessage (vc: self, title: "Alert", message: "your device not supported calling functionality", actionTitles: ["OK"], actions: [{action1 in}, nil])
            }
        }else{
            objUtility.showAlertMessage (vc: self, title: "Alert", message: "your device not supported calling functionality", actionTitles: ["OK"], actions: [{action1 in}, nil])
        }
    }
    /* Website Button Action */
    @IBAction func btnWebsiteTapped () -> Void{
        let objWebview = PlaceDetailsWebViewController.init()
        objWebview.objBusiness = self.objBusiness
        self.navigationController?.pushViewController(objWebview, animated: true)

    }
    /* Distance Button Action */
    @IBAction func btnDistanceTapped () -> Void{
    }
    /* Segment Value Change Event */
    func segmentedValueChanged(_ sender:UISegmentedControl!)
    {
        print("Selected Segment Index is : \(sender.selectedSegmentIndex)")
        if sender.selectedSegmentIndex == 0 {
            self.isReview = false
        }else{
            self.isReview = true
        }
        self.tblDetails?.reloadData()
    }
    /* Tableview Delegate Methods */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isReview == false {
            return 1
        }
        
        return self.objBusiness.arrComments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : PlaceDetailsCell = tableView.dequeueReusableCell(withIdentifier: "DetailsCell") as! PlaceDetailsCell
        
        if isReview == false {
            cell.lblName.text = self.objBusiness.businessBusinessName
            cell.lblAddress.text = self.objBusiness.businessAddress + "," + "\n" + self.objBusiness.businessCity + "," + "\n" + self.objBusiness.businessState + " " + self.objBusiness.businessZipcode
        }else{
            let dict = self.objBusiness.arrComments[indexPath.row] as! NSDictionary
            let user = dict["user"] as! NSDictionary
            cell.lblName.text = user.value(forKey:"name") as? String
            cell.lblAddress.text = dict.value(forKey: "comment") as? String
        }
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
