//
//  PlaceDetailsCell.swift
//  Wamp
//
//  Created by Rameshbhai Patel on 19/11/17.
//  Copyright © 2017 Kushal Patel. All rights reserved.
//

import UIKit

class PlaceDetailsCell: UITableViewCell {
    @IBOutlet var lblName : UILabel!
    @IBOutlet var lblAddress : UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
