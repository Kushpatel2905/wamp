//
//  PlaceListViewController.swift
//  Wamp
//
//  Created by Kushal Patel on 8/16/17.
//  Copyright © 2017 Kushal Patel. All rights reserved.
//

import UIKit

class PlaceListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet var tblList : UITableView?
    @IBOutlet var bottomViewX:NSLayoutConstraint?
    var objSelectedCategory : SubCategory!
    var arrPlace : NSMutableArray!
    var arrARView = [Place]()
    var arViewController: ARViewController!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Place List"
        self.tblList?.register(UINib.init(nibName: "PlaceViewCell", bundle: nil), forCellReuseIdentifier: "PlaceCell")
        tblList?.tableFooterView = UIView.init()
        tblList?.estimatedRowHeight = 100
        tblList?.rowHeight = UITableViewAutomaticDimension;
        
        let rightButtonItem = UIBarButtonItem.init(
            title: "Camera",
            style: .done,
            target: self,
            action: #selector(rightButtonAction(sender:))
        )
        
        self.navigationItem.rightBarButtonItem = rightButtonItem
        
        /*
        let objDataMange = DataManager.init()
        SwiftLoader.show(animated: true)
        objDataMange.getSubCategoryList(strCatId: self.objSelectedCategory.subCatID) { (isSuccess, arrCat) in
            
            DispatchQueue.main.sync {
                self.arrCategory = NSMutableArray.init(array: arrCat!)
                SwiftLoader.hide()
                self.tblList?.reloadData()
            }
        }*/
        
        // Do any additional setup after loading the view.
    }
    
    func rightButtonAction(sender: UIBarButtonItem){
        
        self.arViewController = ARViewController()
        self.arViewController.dataSource = self
        self.arViewController.maxDistance = 0
        self.arViewController.maxVisibleAnnotations = 30
        self.arViewController.maxVerticalLevel = 5
        self.arViewController.headingSmoothingFactor = 0.05
        self.arViewController.trackingManager.userDistanceFilter = 25
        self.arViewController.trackingManager.reloadDistanceFilter = 75
        self.arViewController.setAnnotations(self.arrARView)
        self.arViewController.uiOptions.debugEnabled = false
        self.arViewController.uiOptions.closeButtonEnabled = true
        self.arViewController.navigationController?.isNavigationBarHidden = true
        
        self.arViewController.isSubCategory = true
        self.arViewController.arrBusiness = self.arrPlace
        let navController = UINavigationController(rootViewController: self.arViewController)
        self.present(navController, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.view.addSubview(appDelegate.addBottomView())
        self.bottomViewX?.constant = 70
        let view = self.navigationController?.view.viewWithTag(1001)
        view?.isHidden = false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPlace.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblList?.dequeueReusableCell(withIdentifier: "PlaceCell") as! PlaceViewCell
        let objBusiness = arrPlace.object(at: indexPath.row) as! BusinessList
        cell.lblName.text = objBusiness.businessBusinessName
        cell.lblRate.text = String(objBusiness.businessRatings)
        cell.lblDistance.text = String(objBusiness.businessDistance) + " " + ""
        cell.lblDistance.adjustsFontSizeToFitWidth = false
        cell.lblAddress.text = objBusiness.businessAddress
        return cell;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if #available(iOS 10.0, *) {
            let placeList = PlaceDetailViewController.init()
            placeList.objBusiness = arrPlace.object(at: indexPath.row) as! BusinessList
            self.navigationController?.pushViewController(placeList, animated: true)
        } else {
            // Fallback on earlier versions
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension PlaceListViewController: ARDataSource {
    func ar(_ arViewController: ARViewController, viewForAnnotation: ARAnnotation) -> ARAnnotationView {
        let annotationView = AnnotationView()
        annotationView.annotation = viewForAnnotation
        annotationView.delegate = self
        annotationView.frame = CGRect(x: 0, y: 0, width: 150, height: 50)
        
        return annotationView
    }
}

extension PlaceListViewController: AnnotationViewDelegate {
    func didTouch(annotationView: AnnotationView) {
        if let annotation = annotationView.annotation as? Place {
            let placesLoader = PlacesLoader()
            placesLoader.loadDetailInformation(forPlace: annotation) { resultDict, error in
                
                if let infoDict = resultDict?.object(forKey: "result") as? NSDictionary {
                    annotation.phoneNumber = infoDict.object(forKey: "formatted_phone_number") as? String
                    annotation.website = infoDict.object(forKey: "website") as? String
                    
                   // self.showInfoView(forPlace: annotation)
                }
            }
            
        }
    }
}

