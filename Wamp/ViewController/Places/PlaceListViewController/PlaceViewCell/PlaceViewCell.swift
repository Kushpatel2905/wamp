//
//  PlaceViewCell.swift
//  Wamp
//
//  Created by Kushal Patel on 8/16/17.
//  Copyright © 2017 Kushal Patel. All rights reserved.
//

import UIKit

class PlaceViewCell: UITableViewCell {

    @IBOutlet var lblName : UILabel!
    @IBOutlet var lblRate : UILabel!
    @IBOutlet var lblType : UILabel!
    @IBOutlet var lblAddress : UILabel!
    @IBOutlet var lblDistance : UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
