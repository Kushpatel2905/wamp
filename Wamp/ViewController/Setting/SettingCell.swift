//
//  SettingCell.swift
//  Wamp
//
//  Created by Kushal Patel on 9/12/17.
//  Copyright © 2017 Kushal Patel. All rights reserved.
//

import UIKit

class SettingCell: UITableViewCell {
    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var lblName : UILabel!
    @IBOutlet var lblValue : UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
