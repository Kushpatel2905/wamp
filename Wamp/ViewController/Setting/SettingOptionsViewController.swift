//
//  SettingOptionsViewController.swift
//  Wamp
//
//  Created by Kushal Patel on 9/12/17.
//  Copyright © 2017 Kushal Patel. All rights reserved.
//

import UIKit

class SettingOptionsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet var tblOptions : UITableView?
    var lastSelection: NSIndexPath!
    var selectedOption : NSInteger!
    var arrValues : NSMutableArray! = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblOptions?.register(UINib.init(nibName: "SettingOptionCell", bundle: nil), forCellReuseIdentifier: "SettingOptionCell")
        
        tblOptions?.rowHeight = UITableViewAutomaticDimension
        tblOptions?.estimatedRowHeight = 90
        tblOptions?.tableFooterView = UIView()
        
        if selectedOption == 0{
            arrValues.add("10");
            arrValues.add("20");
            arrValues.add("30");
            arrValues.add("40");
            arrValues.add("50");
            arrValues.add("100");
        }
        
        if selectedOption == 1{
            arrValues.add("Near By Places");
            arrValues.add("100 m / 109 yd");
            arrValues.add("500 m / 546 yd");
        }
        
        if selectedOption == 2{
            arrValues.add("Meter");
            arrValues.add("KiloMeter");
            arrValues.add("Mile");
            
        }
        
        if selectedOption == 3{
            arrValues.add("Keyword");
            arrValues.add("PlaceName");
        }
        

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.view.addSubview(appDelegate.addBottomView())
        let view = self.navigationController?.view.viewWithTag(1001)
        view?.isHidden = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if selectedOption == 0 {
            return 6;
        }
        
        if selectedOption == 1 {
            return 3;
        }
        
        if selectedOption == 2 {
            return 3;
        }
        
        if selectedOption == 3 {
            return 2;
        }
        
        return 4;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : SettingOptionCell = tableView.dequeueReusableCell(withIdentifier: "SettingOptionCell") as! SettingOptionCell
        cell.lblValue.text = arrValues.object(at: indexPath.row) as? String
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension;
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.lastSelection != nil {
            
            let cell : SettingOptionCell = tblOptions?.cellForRow(at: self.lastSelection as IndexPath) as! SettingOptionCell
            cell.imgCheck.isHidden = true;
        }
        
        let cell : SettingOptionCell = tblOptions?.cellForRow(at: indexPath as IndexPath) as! SettingOptionCell
        
        cell.imgCheck.isHidden = false;
        
        self.lastSelection = indexPath as NSIndexPath
        
        tableView.deselectRow(at: indexPath, animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
