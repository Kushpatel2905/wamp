//
//  SettingOptionCell.swift
//  Wamp
//
//  Created by Kushal Patel on 9/12/17.
//  Copyright © 2017 Kushal Patel. All rights reserved.
//

import UIKit

class SettingOptionCell: UITableViewCell {
    @IBOutlet var lblValue : UILabel!
    @IBOutlet var imgCheck : UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
