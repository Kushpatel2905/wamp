//
//  SettingViewController.swift
//  Wamp
//
//  Created by Kushal Patel on 9/12/17.
//  Copyright © 2017 Kushal Patel. All rights reserved.
//

import UIKit

class SettingViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet var tblSettings : UITableView?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Setting"
        tblSettings?.register(UINib.init(nibName: "SettingCell", bundle: nil), forCellReuseIdentifier: "SettingCell")
        tblSettings?.rowHeight = UITableViewAutomaticDimension
        tblSettings?.estimatedRowHeight = 90
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.view.addSubview(appDelegate.addBottomView())
        let view = self.navigationController?.view.viewWithTag(1001)
        view?.isHidden = false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : SettingCell = tableView.dequeueReusableCell(withIdentifier: "SettingCell") as! SettingCell
        
        if indexPath.row == 0 {
            cell.lblTitle.text = "Set Number Of Results"
            cell.lblName.text = "Number of Results"
            cell.lblValue.text = "20"
        }
        
        if indexPath.row == 1 {
            cell.lblTitle.text = "Select The Radius For Place Search"
            cell.lblName.text = "Radius"
            cell.lblValue.text = "1Km/0.62 mi"
        }
        
        if indexPath.row == 2 {
            cell.lblTitle.text = "Distance Unit"
            cell.lblName.text = "Distance Unit"
            cell.lblValue.text = "Kilometer"
        }
        
        if indexPath.row == 3 {
            cell.lblTitle.text = "Select Whether To Search By Keyword or By Place Name"
            cell.lblName.text = "Search Criteria"
            cell.lblValue.text = "Keyword"
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let optionView = SettingOptionsViewController.init()
        optionView.selectedOption = indexPath.row
        self.navigationController?.pushViewController(optionView, animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
