//
//  AppDelegate.swift
//  Wamp
//
//  Created by Kushal Patel on 8/5/17.
//  Copyright © 2017 Kushal Patel. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMobileAds

var appDelegate = UIApplication.shared.delegate as! AppDelegate // Create reference to our app delegate

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,CLLocationManagerDelegate, GADBannerViewDelegate {

    var window: UIWindow?
    var locationManager: CLLocationManager!
    var currentLocation : CLLocation!
    var adBannerView = GADBannerView() // Create our one ADBannerView

    var intHomeCount : NSInteger = 0
    var intAboutUsCount : NSInteger = 0
    var intDirectoryCount : NSInteger = 0
    var intHomeNavigationCount : NSInteger = 0
    var intHomeEventCategoryCount : NSInteger = 0
    var intEventCount : NSInteger = 0
    var intEventDetailsCount : NSInteger = 0
    var intLinkUpCount : NSInteger = 0
    var intBlogCount : NSInteger = 0
    var intShopCount : NSInteger = 0
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        IQKeyboardManager.sharedManager().enable = true

        // Initialize the Google Mobile Ads SDK.
        // Sample AdMob app ID: ca-app-pub-3940256099942544~1458002511
        adBannerView.delegate = self
        adBannerView.isHidden = false
        GADMobileAds.configure(withApplicationID: "ca-app-pub-7634254746329580~5084997880")

        UINavigationBar.appearance().barTintColor = UIColor.init(red: 174.0/255.0, green: 31.0/255.0, blue: 34.0/255.0, alpha: 1.0)
        UINavigationBar.appearance().tintColor = .white
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().shadowImage = UIImage()
        
        if (CLLocationManager.locationServicesEnabled())
        {
            self.locationManager = CLLocationManager()
            self.locationManager.delegate = self
            self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
            self.locationManager.distanceFilter = 10.0  // Movement threshold for new events
        }
        if CLLocationManager.authorizationStatus() != .authorizedWhenInUse {
            locationManager.requestWhenInUseAuthorization()
        }else{
            locationManager.startUpdatingLocation()
        }
        return true
    }
    private func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        switch status {
        case .authorizedWhenInUse:
            manager.startUpdatingLocation()
            break
        case .authorizedAlways:
            manager.startUpdatingLocation()
            break
        case .denied:
            //handle denied
            break
        case .notDetermined:
            manager.requestWhenInUseAuthorization()
            break
        default:
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        currentLocation = locations.last! as CLLocation
        for location in locations {
            print("**********************")
            print("Long \(location.coordinate.longitude)")
            print("Lati \(location.coordinate.latitude)")
            print("Alt \(location.altitude)")
            print("Sped \(location.speed)")
            print("Accu \(location.horizontalAccuracy)")
            print("**********************")
        }
       // currentLocation:CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(currentLocation.coordinate.latitude) \(currentLocation.coordinate.longitude)")
    }
    func locationManager(_ manager: CLLocationManager,
                         didFailWithError error: Error){
        currentLocation = CLLocation(latitude: 23.0862184, longitude: 72.56407)

    }
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    
    
   public func addBottomView() -> UIView {
//    self.adBannerView = GADBannerView(adSize: kGADAdSizeBanner)
    self.adBannerView.frame = CGRect(x: 0, y: UIScreen.main.bounds.size.height - 70, width: UIScreen.main.bounds.size.width, height: 70)
    self.adBannerView.center = CGPoint(x: UIScreen.main.bounds.size.width/2,y: UIScreen.main.bounds.size.height - self.adBannerView.frame.height / 2)
    //view.frame.height - appDelegate.adBannerView.frame.height / 2
    self.adBannerView.adUnitID = "ca-app-pub-7634254746329580/7027978403"
    self.adBannerView.rootViewController = self.window?.rootViewController
    self.adBannerView.load(GADRequest())
    return adBannerView
    }

    public func convertDateString(dateString : String!, fromFormat sourceFormat : String!, toFormat desFormat : String!) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = sourceFormat
        let date = dateFormatter.date(from: dateString)
        
        dateFormatter.dateFormat = desFormat
        
        return dateFormatter.string(from: date!)
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

