//
//  ViewController.swift
//  Wamp
//
//  Created by Kushal Patel on 8/5/17.
//  Copyright © 2017 Kushal Patel. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet var bottomViewX:NSLayoutConstraint?
    @IBOutlet var txtUsername : UITextField!
    @IBOutlet var txtPassword : UITextField!
    @IBOutlet var btnRegister : UIButton!
    @IBOutlet var btnListBusiness : UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        txtUsername.text = "test1@fameitc.com"//"nirav@fameitc.com"
        txtPassword.text = "123"
//        self.imgBaseLineConstraint.constant = (UIScreen.main.bounds.height * 25) / 414
        // Do any additional setup after loading the view, typically from a nib.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = true
        self.view.addSubview(appDelegate.addBottomView())
        bottomViewX?.constant = 70;
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /* Login Button Action */
    @IBAction func btnLoginClicked() -> Void {
        //let objPlace : PlaceMapViewController = PlaceMapViewController.init()
        //self.navigationController?.pushViewController(objPlace, animated: true)
        
        //return
        
        if txtUsername.text == "" || txtPassword.text == "" {
            let alertView = UIAlertController(title: "Login", message: "Please enter username or password", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                
            })
            alertView.addAction(action)
            self.present(alertView, animated: true, completion: nil)
            
            return
        }
        SwiftLoader.show(animated: true)
        let objDatamange = DataManager.init();
        
        objDatamange.doLogin(strUserName: txtUsername.text!, strPassword: txtPassword.text!) { (isSuccess, dictResponse) in
            DispatchQueue.main.sync {
                let dict = dictResponse as NSDictionary!
                let status : Bool = dict!["status"] as! Bool
                if status == true {
                    let dataUser : NSDictionary = dict!["data"] as! NSDictionary
                    if dataUser .isKind(of: NSDictionary.self){
                        let dictUser : NSDictionary = dataUser["user"] as! NSDictionary
                        if dictUser .isKind(of: NSDictionary.self){
                            User.sharedInstance.userID = String(dictUser.value(forKey: "id") as! NSInteger)
                            User.sharedInstance.userName = dictUser.value(forKey: "name") as! String
                            User.sharedInstance.userEmail = dictUser.value(forKey: "email") as! String
                            
                            User.sharedInstance.userAvtar = dictUser.value(forKey: "avatar") as! String
                            User.sharedInstance.userCreateDate = dictUser.value(forKey: "created_at") as! String
                            User.sharedInstance.userUpdateDate = dictUser.value(forKey: "updated_at") as! String
                            User.sharedInstance.userToken = dataUser.value(forKey: "token") as! String!
                            let objHome : HomeViewController = HomeViewController.init()
                            self.navigationController?.pushViewController(objHome, animated: true)
                        }
                    }
                }
                
                /*if ((dict?.value(forKey: "data")) != nil){
                    User.sharedInstance.userToken = dictResponse?.value(forKey: "auth_key") as! String!
                    let objHome : HomeViewController = HomeViewController.init()
                    self.navigationController?.pushViewController(objHome, animated: true)
                }*/
                SwiftLoader.hide()
            }
            
        }
        
    }
    
    /* Create an New Account click event */
    
    @IBAction func btnCreateAnAccount() -> Void{
        let objRegister : CreateAccountViewController = CreateAccountViewController.init()
        self.navigationController?.pushViewController(objRegister, animated: true)
        
    }

}

