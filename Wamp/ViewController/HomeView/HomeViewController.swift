//
//  HomeViewController.swift
//  Wamp
//
//  Created by Kushal Patel on 8/10/17.
//  Copyright © 2017 Kushal Patel. All rights reserved.
//

import UIKit
import SDWebImage
import CoreLocation
import MapKit

class HomeViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    // Call for augmented reality object
    fileprivate var Businessplaces = [Place]()
    var arViewController: ARViewController!

   @IBOutlet var collView : UICollectionView?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collView?.register(UINib.init(nibName: "HomeViewCell", bundle: nil), forCellWithReuseIdentifier:"HomeCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
//        if  UserDefaults.standard.integer(forKey: "intHomeCount") == appDelegate.intHomeCount  {
//            
//        }
//        appDelegate.intHomeCount =
//        UserDefaults.standard.set(appDelegate.intHomeCount++, forKey: "Key")  //Integer
        self.navigationController?.isNavigationBarHidden = true
        self.view.addSubview(appDelegate.addBottomView())
        let view = self.navigationController?.view.viewWithTag(1001)
        view?.isHidden = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6;
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return  CGSize.init(width: UIScreen.main.bounds.size.width/2, height: 185)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCell", for: indexPath) as! HomeViewCell
        
        if indexPath.row == 0{
            cell.imgIcon.image = UIImage.init(named: "directory")
            cell.lblTitle.text = "Directory";
        }
        
        if indexPath.row == 1{
            cell.imgIcon.image = UIImage.init(named: "mapCat")
            cell.lblTitle.text = "Navigate";
        }
        
        if indexPath.row == 2{
            cell.imgIcon.image = UIImage.init(named: "eventCat")
            cell.lblTitle.text = "Events";
        }
        
        if indexPath.row == 3{
            cell.imgIcon.image = UIImage.init(named: "peopleCat")
            cell.lblTitle.text = "Link";
        }
        
        if indexPath.row == 4{
            cell.imgIcon.image = UIImage.init(named: "event1Cat")
            cell.lblTitle.text = "Blog";
        }
        
        if indexPath.row == 5{
            cell.imgIcon.image = UIImage.init(named: "shopCat")
            cell.lblTitle.text = "The Out Shop";
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let objCategory = CategoryViewController.init()
            self.navigationController?.pushViewController(objCategory, animated: true)
        }
        else if indexPath.row == 1 {
            let nearMe = PlaceMapViewController.init()
            self.navigationController?.pushViewController(nearMe, animated: true)
            let view = self.navigationController?.view.viewWithTag(1001)
            view?.isHidden = true
            
        }else if indexPath.row == 2 {
            let objEventCat = EventByCategoryViewController.init()
            self.navigationController?.pushViewController(objEventCat, animated: true)
            let view = self.navigationController?.view.viewWithTag(1001)
            view?.isHidden = true
        }else if indexPath.row == 3 {
            let objLinkVc = LinkViewController.init()
            self.navigationController?.pushViewController(objLinkVc, animated: true)
            let view = self.navigationController?.view.viewWithTag(1001)
            view?.isHidden = true
        }else{
            let ObjWebView = WebViewController.init()
            if indexPath.row == 4 {
                ObjWebView.title = "BLOG"
                ObjWebView.strURL = URLNAME.BLOG.rawValue
            }else{
                ObjWebView.title = "The Out Shop"
                ObjWebView.strURL = URLNAME.SHOP.rawValue
            }
            self.navigationController?.pushViewController(ObjWebView, animated: true)
            let view = self.navigationController?.view.viewWithTag(1001)
            view?.isHidden = true
        }
    }
    
    func showInfoView(forPlace place: Place) {
        let alert = UIAlertController(title: place.placeName , message: place.infoText, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        arViewController.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func btnLogoutClicked() -> Void {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func btnAboutUsTapped() -> Void {
        let objAbout = AboutUsViewController.init()
        self.navigationController?.pushViewController(objAbout, animated: true)
        let view = self.navigationController?.view.viewWithTag(1001)
        view?.isHidden = true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension HomeViewController: ARDataSource {
    func ar(_ arViewController: ARViewController, viewForAnnotation: ARAnnotation) -> ARAnnotationView {
        let annotationView = AnnotationView()
        annotationView.annotation = viewForAnnotation
        annotationView.delegate = self
        annotationView.frame = CGRect(x: 0, y: 0, width: 150, height: 50)
        
        return annotationView
    }
}

extension HomeViewController: AnnotationViewDelegate {
    func didTouch(annotationView: AnnotationView) {
        if let annotation = annotationView.annotation as? Place {
            let placesLoader = PlacesLoader()
            placesLoader.loadDetailInformation(forPlace: annotation) { resultDict, error in
                
                if let infoDict = resultDict?.object(forKey: "result") as? NSDictionary {
                    annotation.phoneNumber = infoDict.object(forKey: "formatted_phone_number") as? String
                    annotation.website = infoDict.object(forKey: "website") as? String
                    
                    self.showInfoView(forPlace: annotation)
                }
            }
            
        }
    }
}

