//
//  HomeViewCell.swift
//  Wamp
//
//  Created by Kushal Patel on 8/10/17.
//  Copyright © 2017 Kushal Patel. All rights reserved.
//

import UIKit

class HomeViewCell: UICollectionViewCell {

    @IBOutlet var imgIcon : UIImageView!
    @IBOutlet var lblTitle : UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
