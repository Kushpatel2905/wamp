//
//  WebViewController.swift
//  Wamp
//
//  Created by Rameshbhai Patel on 11/01/18.
//  Copyright © 2018 Kushal Patel. All rights reserved.
//

import UIKit
enum URLNAME: String {
    case SUGGESTIONS = "http://blog.fameitc.com/"
    case BLOG = "http://fameitc.com/outinthecity/blog/"
    case SHOP = "http://fameitc.com/outinthecity/shop/"
    case TWITTER = "https://www.twitter.com/outinthecities/"
    case FACEBOOK = "https://www.facebook.com/outinthecities/"
    case INSTAGRAM = "https://www.instagram.com/outinthecities/"
    case PINTEREST = "https://www.pinterest.com/outinthecities/"
    case PRIVACY = "http://fameitc.com/mobile-apps-development"
    case LICENSEAGREEMENT = "http://fameitc.com/contact"
    case ABOUTDEVELOPER = "http://fameitc.com/web-application-development"
}
class WebViewController: UIViewController,UIWebViewDelegate {
    public var strURL = String()
    @IBOutlet var webView : UIWebView?
    @IBOutlet var bottomViewX:NSLayoutConstraint?

    override func viewDidLoad() {
        super.viewDidLoad()
        SwiftLoader.show(animated: true)
        webView?.delegate = self
        if let url = URL(string: strURL) {
            let request = URLRequest(url: url)
            webView?.loadRequest(request)
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.view.addSubview(appDelegate.addBottomView())
        self.bottomViewX?.constant = 70
        let view = self.navigationController?.view.viewWithTag(1001)
        view?.isHidden = false
    }
    func webViewDidStartLoad(_ webView: UIWebView) {
        
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error)
    {
        SwiftLoader.hide()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        SwiftLoader.hide()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
