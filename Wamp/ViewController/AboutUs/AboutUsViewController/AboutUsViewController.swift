//
//  AboutUsViewController.swift
//  Wamp
//
//  Created by Rameshbhai Patel on 11/01/18.
//  Copyright © 2018 Kushal Patel. All rights reserved.
//

import UIKit

class AboutUsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet var tblAboutUs : UITableView?
    @IBOutlet var bottomViewX:NSLayoutConstraint?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "About Us"
        
        tblAboutUs?.register(UINib.init(nibName: "SettingOptionCell", bundle: nil), forCellReuseIdentifier: "SettingOptionCell")
        
        tblAboutUs?.register(UINib.init(nibName: "AboutUsCell", bundle: nil), forCellReuseIdentifier: "AboutUsCell")
        
        tblAboutUs?.sectionHeaderHeight = 50
        
        tblAboutUs?.rowHeight = UITableViewAutomaticDimension
        tblAboutUs?.estimatedRowHeight = 90
        tblAboutUs?.dataSource = self
        tblAboutUs?.delegate = self
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.view.addSubview(appDelegate.addBottomView())
        self.bottomViewX?.constant = 70
        let view = self.navigationController?.view.viewWithTag(1001)
        view?.isHidden = false
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch (section) {
        case 0:
            return 2
        case 1:
            return 4
        case 2:
            return 3
        case 3:
            return 0
        default:
            return 0
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell : SettingOptionCell = tableView.dequeueReusableCell(withIdentifier: "SettingOptionCell") as! SettingOptionCell
        cell.backgroundColor = UIColor.init(red: 219.0/255.0, green: 219.0/255.0, blue: 219.0/255.0, alpha: 1.0)
        if section == 0 {
            cell.lblValue.text = "Share"
        }else if section == 1 {
            cell.lblValue.text = "Follow Us!"
        }else if section == 2 {
            cell.lblValue.text = "Resources"
        }else{
            let nsObject: AnyObject? = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as AnyObject
            let version = nsObject as! String
            cell.lblValue.text = String("App Version : " + version)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : AboutUsCell = tableView.dequeueReusableCell(withIdentifier: "AboutUsCell") as! AboutUsCell
        if indexPath.section == 0{
            if indexPath.row == 0 {
                cell.lblTitle?.text = "Tell your friends"
                cell.imgIcon?.image = UIImage.init(named: "share")
            }else{
                cell.lblTitle?.text = "Suggestions"
                cell.imgIcon?.image = UIImage.init(named: "suggestion")
            }
        }else if indexPath.section == 1{
            if indexPath.row == 0 {
                cell.lblTitle?.text = "Twitter"
                cell.imgIcon?.image = UIImage.init(named: "twitter")
            }else if indexPath.row == 1 {
                cell.lblTitle?.text = "Facebook"
                cell.imgIcon?.image = UIImage.init(named: "fb")
            }else if indexPath.row == 2 {
                cell.lblTitle?.text = "Instagram"
            }else{
                cell.lblTitle?.text = "Pinterest"
                cell.imgIcon?.image = UIImage.init(named: "pintrest")
            }
        }else{
            if indexPath.row == 0 {
                cell.lblTitle?.text = "Privacy Policy"
                cell.imgIcon?.image = UIImage.init(named: "policy")
            }else if indexPath.row == 1 {
                cell.lblTitle?.text = "End User License Agreement"
                cell.imgIcon?.image = UIImage.init(named: "licence")
            }else{
                cell.lblTitle?.text = "About Developer"
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let ObjWebView = WebViewController.init()
        if indexPath.section == 0{
            if indexPath.row == 0 {
                let text = "Download app https://play.google.com/store/apps/details?id=com.outinthecity"
                let textToShare = [ text ]
                let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view // so
                activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
                self.present(activityViewController, animated: true, completion: nil)
                return
            }else{
                ObjWebView.title = "SUGGESTIONS"
                ObjWebView.strURL = URLNAME.SUGGESTIONS.rawValue
            }
        }else if indexPath.section == 1{
            if indexPath.row == 0 {
                ObjWebView.title = "TWITTER"
                ObjWebView.strURL = URLNAME.TWITTER.rawValue
            }else if indexPath.row == 1 {
                ObjWebView.title = "FACEBOOK"
                ObjWebView.strURL = URLNAME.FACEBOOK.rawValue
            }else if indexPath.row == 2 {
                ObjWebView.title = "INSTAGRAM"
                ObjWebView.strURL = URLNAME.INSTAGRAM.rawValue
            }else{
                ObjWebView.title = "PINTEREST"
                ObjWebView.strURL = URLNAME.PINTEREST.rawValue
            }
        }else{
            if indexPath.row == 0 {
                ObjWebView.title = "PRIVACY POLICY"
                ObjWebView.strURL = URLNAME.PRIVACY.rawValue
            }else if indexPath.row == 1 {
                ObjWebView.title = "LICENSE AGREEMENT"
                ObjWebView.strURL = URLNAME.LICENSEAGREEMENT.rawValue
            }else{
                ObjWebView.title = "ABOUT DEVELOPER"
                ObjWebView.strURL = URLNAME.ABOUTDEVELOPER.rawValue
            }
        }
        self.navigationController?.pushViewController(ObjWebView, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
