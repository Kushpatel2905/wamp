//
//  BusinessList.swift
//  Wamp
//
//  Created by Rameshbhai Patel on 16/11/17.
//  Copyright © 2017 Kushal Patel. All rights reserved.
//

import UIKit
import CoreLocation

class BusinessList: NSObject {
    var businessID : String!
    var businessEmail : String!
    var businessFirstname : String!
    var businessLastname : String!
    var businessBusinessName : String!
    var businessTypeID : String!
    var businessSubCatID : String!
    var businessPriceID : String!
    var businessContact : String!
    var businessWebSite : String!
    var businessAddress : String!
    var businessState : String!
    var businessCity : String!
    var businessZipcode : String!
    var businessLogo : String!
    var businessDescription : String!
    var businessLatitude : String!
    var businessLongitude : String!
    var businessDistance : String!
    var businessStatus : String!
    var businessCreatedDate : String!
    var businessUpdatedDate : String!
    var businessRatings : String!
    var arrComments : NSMutableArray!
//    var places = [Place]()

    override init () {
        // uncomment this line if your class has been inherited from any other class
        //super.init()
    }
    
    //
    
    convenience init(_ dictionary: NSDictionary?) {
        self.init()
        self.businessID = String(dictionary?.value(forKey: "id") as! NSInteger)
        self.businessEmail = dictionary?.value(forKey: "email") as! String
        self.businessFirstname = dictionary?.value(forKey: "firstname") as! String
        self.businessLastname = dictionary?.value(forKey: "lastname") as! String
        self.businessBusinessName = dictionary?.value(forKey: "businessname") as! String
        self.businessTypeID = String(dictionary?.value(forKey: "type_id") as! NSInteger)
        self.businessSubCatID = String(dictionary?.value(forKey: "category_id") as! NSInteger)
        self.businessPriceID = String(dictionary?.value(forKey: "price_id") as! NSInteger)
        self.businessContact = dictionary?.value(forKey: "contact") as! String
        
        if let website = dictionary!["website"] as? String {
            self.businessWebSite = website
        }
        else{
            self.businessWebSite=""
        }
        
        
        self.businessAddress = dictionary?.value(forKey: "address") as! String
        self.businessState = dictionary?.value(forKey: "state") as! String
        self.businessCity = dictionary?.value(forKey: "city") as! String
        self.businessZipcode = String(dictionary?.value(forKey: "zip") as! NSInteger)
        self.businessLogo = dictionary?.value(forKey: "logo") as! String
        self.businessDescription = dictionary?.value(forKey: "description") as! String
        self.businessLatitude = String(dictionary?.value(forKey: "lat") as! Double)
        self.businessLongitude = String(dictionary?.value(forKey: "lng") as! Double)
        self.businessDistance = dictionary?.value(forKey: "distance") as! String
        self.businessStatus = String(dictionary?.value(forKey: "status") as! Bool)
       // self.businessCreatedDate = dictionary?.value(forKey: "created_at") as! String
       // self.businessUpdatedDate = dictionary?.value(forKey: "updated_at") as! String
        if dictionary?.value(forKey: "ratings") is NSNull {
            self.businessRatings = "0.0"
        }else{
            self.businessRatings = String(dictionary?.value(forKey: "ratings") as! Double)
        }

//        let reference = placeDict.object(forKey: "reference") as! String
//        let name = placeDict.object(forKey: "name") as! String
//        let address = placeDict.object(forKey: "vicinity") as! String
//
//        let location = CLLocation(latitude: latitude, longitude: longitude)
//        let place = Place(location: location, reference: reference, name: name, address: address)
    }
    
}
