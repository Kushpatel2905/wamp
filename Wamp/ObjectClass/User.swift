//
//  User.swift
//  Wamp
//
//  Created by Kushal Patel on 10/15/17.
//  Copyright © 2017 Kushal Patel. All rights reserved.
//

import UIKit

class User: NSObject {
    var userID : String!
    var userName : String!
    var userEmail : String!
    var userAvtar : String!
    var userCreateDate : String!
    var userUpdateDate : String!
    var userToken : String!
    
    static let sharedInstance: User = {
        let instance = User()
        // setup code
        return instance
    }()
    
    override init () {
        // uncomment this line if your class has been inherited from any other class
        //super.init()
    }
    
    //
    
    convenience init(_ dictionary: NSDictionary?) {
        self.init()
        self.userID = String(dictionary?.value(forKey: "id") as! NSInteger)
        self.userName = dictionary?.value(forKey: "name") as! String
        self.userEmail = dictionary?.value(forKey: "email") as! String
        
        self.userAvtar = dictionary?.value(forKey: "avatar") as! String
        self.userCreateDate = dictionary?.value(forKey: "created_at") as! String
        self.userUpdateDate = dictionary?.value(forKey: "updated_at") as! String
    }
}
