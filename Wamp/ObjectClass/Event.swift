//
//  Event.swift
//  Wamp
//
//  Created by Rameshbhai Patel on 07/01/18.
//  Copyright © 2018 Kushal Patel. All rights reserved.
//

import UIKit
class EventCategory: NSObject {
    var eventCatID : String!
    var eventCatName : String!
    var eventCatURL : String!
    var eventCatCreatedAt : String!
    var eventCatUpdatedAt : String!
    override init () {
        // uncomment this line if your class has been inherited from any other class
        //super.init()
    }
    
    //
    convenience init(_ dictionary: NSDictionary?) {
        self.init()
        self.eventCatID = String(dictionary?.value(forKey: "id") as! NSInteger)
        self.eventCatName = dictionary?.value(forKey: "event_category") as! String
        self.eventCatURL = dictionary?.value(forKey: "image") as! String
        self.eventCatCreatedAt = dictionary?.value(forKey: "created_at") as! String
        self.eventCatUpdatedAt = dictionary?.value(forKey: "updated_at") as! String
    }
}


class Event: NSObject {
    var eventID : String!
    var eventName : String!
    var eventDate : String!
    var eventLocation : String!
    var eventCategoryId : String!
    var eventType : String!
    var eventCrowdId : String!
    var eventTime : String!
    var eventBanner : String!
    var eventLogo : String!
    var eventDescription : String!
    var eventRegisterLimit : String!
    var eventCreatedAt : String!
    var eventUpdatedAt : String!
    var eventHasLinked : String!
    override init () {
        // uncomment this line if your class has been inherited from any other class
        //super.init()
    }
    
    //
    
    convenience init(_ dictionary: NSDictionary?) {
        self.init()
        self.eventID = String(dictionary?.value(forKey: "id") as! NSInteger)
        self.eventName = dictionary?.value(forKey: "event_name") as! String
        self.eventDate = dictionary?.value(forKey: "event_date") as! String
        
        self.eventLocation = dictionary?.value(forKey: "location") as! String
        self.eventCategoryId = String(dictionary?.value(forKey: "event_category_id") as! NSInteger)
        self.eventType = dictionary?.value(forKey: "event_type") as! String
        
        self.eventCrowdId = String(dictionary?.value(forKey: "event_crowd_id") as! NSInteger)
        self.eventTime = dictionary?.value(forKey: "event_time") as! String
        self.eventBanner = dictionary?.value(forKey: "event_banner") as! String
        
        self.eventLogo = dictionary?.value(forKey: "event_logo") as! String
        self.eventDescription = dictionary?.value(forKey: "description") as! String
        self.eventRegisterLimit = String(dictionary?.value(forKey: "register_limit") as! NSInteger)
        
        self.eventCreatedAt = dictionary?.value(forKey: "created_at") as! String
        self.eventUpdatedAt = dictionary?.value(forKey: "updated_at") as! String
        self.eventHasLinked = String(dictionary?.value(forKey: "hasLinked") as! Bool)
    }
}

