//
//  SubCategory.swift
//  Wamp
//
//  Created by Kushal Patel on 10/17/17.
//  Copyright © 2017 Kushal Patel. All rights reserved.
//

import UIKit

class SubCategory: NSObject {
    var subCatID : String!
    var subCatTypeID : String!
    var subCatName : String!
    var subCatImageURL : String!
    var subCatStatus : String!
    var subCatCreateDate : String!
    var subCatUpdateDate : String!
    
    var typeID : String!
    var name : String!
    
    
    override init () {
        // uncomment this line if your class has been inherited from any other class
        //super.init()
    }
    
    //
    
    convenience init(_ dictionary: NSDictionary?) {
        self.init()
        self.subCatID = String(dictionary?.value(forKey: "id") as! NSInteger)
        self.subCatTypeID = String(dictionary?.value(forKey: "type_id") as! NSInteger)
        self.subCatName = dictionary?.value(forKey: "name") as! String
        self.subCatImageURL = dictionary?.value(forKey: "image") as! String
        
        self.subCatStatus = String(dictionary?.value(forKey: "status") as! Bool)
        self.subCatCreateDate = dictionary?.value(forKey: "created_at") as! String
        self.subCatUpdateDate = dictionary?.value(forKey: "updated_at") as! String
    }
    
}
