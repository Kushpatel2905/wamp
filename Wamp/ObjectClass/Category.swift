//
//  Category.swift
//  Wamp
//
//  Created by Kushal Patel on 10/16/17.
//  Copyright © 2017 Kushal Patel. All rights reserved.
//

import UIKit

class Category: NSObject {
    var catID : String!
    var catResturantName : String!
    var catImageURL : String!
    var catStatus : String!
    var catCreateDate : String!
    var catUpdateDate : String!
    var arrSubCategory = NSMutableArray()
    override init () {
        // uncomment this line if your class has been inherited from any other class
        //super.init()
    }
    
    //
    
    convenience init(_ dictionary: NSDictionary?) {
        self.init()
        self.catID = String(dictionary?.value(forKey: "id") as! NSInteger) 
        self.catResturantName = dictionary?.value(forKey: "restaurant_type") as! String
        self.catImageURL = dictionary?.value(forKey: "image") as! String
        
        self.catStatus = String(dictionary?.value(forKey: "status") as! Bool)
        self.catCreateDate = dictionary?.value(forKey: "created_at") as! String
        self.catUpdateDate = dictionary?.value(forKey: "updated_at") as! String
        self.arrSubCategory = NSMutableArray.init()
        print(dictionary!["categories"] as Any)
        for dict in dictionary!["categories"] as! NSArray{
            let objSubCat : SubCategory = SubCategory.init((dict as AnyObject as! NSDictionary) )
            self.arrSubCategory.add(objSubCat)
        }
        print(self.arrSubCategory)
    }
}
