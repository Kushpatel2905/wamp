//
//  DataManager.swift
//  Wamp
//
//  Created by Kushal Patel on 10/13/17.
//  Copyright © 2017 Kushal Patel. All rights reserved.
//

import UIKit

class DataManager: NSObject {
    func doLogin(strUserName:String,strPassword:String, completionHandler:@escaping (_ success :Bool,_ data: NSDictionary?) -> Void){
        // Set up the URL request
        let todoEndpoint: String = "http://fameitc.com/outinthecity/api/v1/auth/login"
        guard let url = URL(string: todoEndpoint) else {
            print("Error: cannot create URL")
            return
        }
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "POST"
        let paramString = String(format:"email=%@&password=%@",strUserName,strPassword)
        urlRequest.httpBody = paramString.data(using: String.Encoding.utf8)
        
        // set up the session
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        // make the request
        let task = session.dataTask(with: urlRequest) {
            (data, response, error) in
            
            guard error == nil else {
                print("error calling GET on /todos/1")
                print(error ?? "")
                return
            }
            
//            print(String.init(data: data!, encoding:String.Encoding.utf8) as Any)
            // make sure we got data
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            // parse the result as JSON, since that's what the API provides
            do {
                guard let todo = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String: AnyObject] else {
                    print("error trying to convert data to JSON")
                    return
                }
                print(todo)
                completionHandler(true,todo as NSDictionary)
                
            } catch  {
                print("error trying to convert data to JSON")
                completionHandler(false,NSDictionary.init())
                return
            }
            
        }
        
        task.resume()
    }
    
    /*  Params Name for Register
        username:jack12
        password:12345678
        email:jack12@gmail.com
        name:Jack
 */
    func doRegister(dictRegister:NSMutableDictionary, completionHandler:@escaping (_ success :Bool,_ data: NSDictionary?) -> Void){
        // Set up the URL request
        let todoEndpoint: String = "http://fameitc.com/outinthecity/api/v1/auth/register"
        guard let url = URL(string: todoEndpoint) else {
            print("Error: cannot create URL")
            return
        }
        let strName : NSString!
        let strUserName : NSString!
        let strEmail : NSString!
        let strPassword : NSString!
        
        strName = dictRegister.value(forKey: "Name") as! NSString
        strUserName = dictRegister.value(forKey: "UserName") as! NSString
        strEmail = dictRegister.value(forKey: "Email") as! NSString
        strPassword = dictRegister.value(forKey: "Password") as! NSString
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "POST"
        let paramString = String(format:"username=%@&password=%@&email=%@&name=%@",strUserName,strPassword,strEmail,strName)
        
        print(paramString)
        urlRequest.httpBody = paramString.data(using: String.Encoding.utf8)
        
        // set up the session
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        // make the request
        let task = session.dataTask(with: urlRequest) {
            (data, response, error) in
            
            guard error == nil else {
                print("error calling GET on /todos/1")
                print(error ?? "")
                return
            }
            
            print(String.init(data: data!, encoding:String.Encoding.utf8) as Any)
            // make sure we got data
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            // parse the result as JSON, since that's what the API provides
            do {
                guard let todo = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String: AnyObject] else {
                    print("error trying to convert data to JSON")
                    return
                }
                
                completionHandler(true,todo as NSDictionary)
                
            } catch  {
                print("error trying to convert data to JSON")
                completionHandler(false,NSDictionary.init())
                return
            }
            
        }
        
        task.resume()
    }

    func getCategoryList(completionHandler:@escaping (_ success :Bool,_ data: NSMutableArray?) -> Void){
        // Set up the URL request
        let todoEndpoint: String = "http://fameitc.com/outinthecity/api/v1/categories/subcategories/all"
        guard let url = URL(string: todoEndpoint) else {
            print("Error: cannot create URL")
            return
        }
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "GET"
        urlRequest.setValue("Bearer " + User.sharedInstance.userToken, forHTTPHeaderField: "Authorization")
        
        // set up the session
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        // make the request
        let task = session.dataTask(with: urlRequest) {
            (data, response, error) in
            
            guard error == nil else {
                print("error calling GET on /todos/1")
                print(error ?? "")
                return
            }
            
            //print(String.init(data: data!, encoding:String.Encoding.utf8) as Any)
            // make sure we got data
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            // parse the result as JSON, since that's what the API provides
            do {
                guard let todo = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String: AnyObject] else {
                    print("error trying to convert data to JSON")
                    return
                }
                print(todo)

                let status : Bool = todo["status"] as! Bool
                print(status)
                if status == true{
                        let arrCat = NSMutableArray.init()
                        for dict in todo["data"] as! NSArray{
                            let objCat : Category = Category.init((dict as AnyObject as! NSDictionary) )
                            arrCat.add(objCat)
                        }
                        completionHandler(true,arrCat as NSMutableArray)
                }else{
                    completionHandler(false,NSMutableArray.init())
                }
                
            } catch  {
                print("error trying to convert data to JSON")
                completionHandler(false,NSMutableArray.init())
                return
            }
            
        }
        
        task.resume()
    }
    
    /*func getSubCategoryList(strCatId:String,completionHandler:@escaping (_ success :Bool,_ data: NSMutableArray?) -> Void){
        // Set up the URL request
        let todoEndpoint: String = "http://fameitc.com/outinthecity/api/categorytype/" + strCatId
        guard let url = URL(string: todoEndpoint) else {
            print("Error: cannot create URL")
            return
        }
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "GET"
        urlRequest.setValue(User.sharedInstance.userToken, forHTTPHeaderField: "Authorization")
        
        // set up the session
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        // make the request
        let task = session.dataTask(with: urlRequest) {
            (data, response, error) in
            
            guard error == nil else {
                print("error calling GET on /todos/1")
                print(error ?? "")
                return
            }
            
            print(String.init(data: data!, encoding:String.Encoding.utf8) as Any)
            // make sure we got data
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            // parse the result as JSON, since that's what the API provides
            do {
                guard let todo = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String: AnyObject] else {
                    print("error trying to convert data to JSON")
                    return
                }
                
                let arrCat = NSMutableArray.init()
                
                for dict in todo["data"] as! NSArray{
                    
                    let objCat : SubCategory = SubCategory.init((dict as AnyObject as! NSDictionary) )
                    arrCat.add(objCat)
                }
                
                completionHandler(true,arrCat as NSMutableArray)
                
            } catch  {
                print("error trying to convert data to JSON")
                completionHandler(false,NSMutableArray.init())
                return
            }
            
        }
        
        task.resume()
    }
    */
    
    
    
    func getEstablishmentDetails(strSubCatId:String,completionHandler:@escaping (_ success :Bool,_ data: NSMutableDictionary?) -> Void){
        
        
        // Set up the URL request
        let todoEndpoint: String = "http://fameitc.com/outinthecity/api/v1/establishment/get"
        guard let url = URL(string: todoEndpoint) else {
            print("Error: cannot create URL")
            return
        }
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "POST"
        let paramString = String(format:"establishment_id=%@",strSubCatId)
        print(paramString)
        urlRequest.httpBody = paramString.data(using: String.Encoding.utf8)
        urlRequest.setValue("Bearer " + User.sharedInstance.userToken, forHTTPHeaderField: "Authorization")
        
        // set up the session
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        // make the request
        let task = session.dataTask(with: urlRequest) {
            (data, response, error) in
            
            guard error == nil else {
                print("error calling GET on /todos/1")
                print(error ?? "")
                return
            }
            
            print(String.init(data: data!, encoding:String.Encoding.utf8) as Any)
            // make sure we got data
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            // parse the result as JSON, since that's what the API provides
            do {
                guard let todo = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String: AnyObject] else {
                    print("error trying to convert data to JSON")
                    return
                }
                let status : Bool = todo["status"] as! Bool
                print(status)
                if status == true{
                    completionHandler(true,NSMutableDictionary.init(dictionary: todo["data"] as! NSDictionary))
                }else{
                    completionHandler(false,NSMutableDictionary.init())
                }
                /*
                 let arrBusiness = NSMutableArray.init()
                 
                 for dict in todo["data"] as! NSArray{
                 let objBusiness : BusinessList = BusinessList.init((dict as AnyObject as! NSDictionary) )
                 arrBusiness.add(objBusiness)
                 }
                 
                 completionHandler(true,arrBusiness as NSMutableArray)*/
                
            } catch  {
                print("error trying to convert data to JSON")
                completionHandler(false,NSMutableDictionary.init())
                return
            }
            
        }
        
        task.resume()
    }
    
    
    func getBusinessList(strSubCatId:String,completionHandler:@escaping (_ success :Bool,_ data: NSMutableArray?) -> Void){
        let myLatitude: String = String(format: "%f", (appDelegate.currentLocation.coordinate.latitude))
        let myLongitude: String = String(format:"%f", (appDelegate.currentLocation.coordinate.longitude))
        print(myLongitude)
        
        // Set up the URL request
        let todoEndpoint: String = "http://fameitc.com/outinthecity/api/v1/subcategory/establishments"
        guard let url = URL(string: todoEndpoint) else {
            print("Error: cannot create URL")
            return
        }
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "POST"
        let paramString = String(format:"subcategory_id=%@&lat=%@&long=%@","42",myLatitude,myLongitude)
        print(paramString)
        urlRequest.httpBody = paramString.data(using: String.Encoding.utf8)
        urlRequest.setValue("Bearer " + User.sharedInstance.userToken, forHTTPHeaderField: "Authorization")

        // set up the session
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        // make the request
        let task = session.dataTask(with: urlRequest) {
            (data, response, error) in
            
            guard error == nil else {
                print("error calling GET on /todos/1")
                print(error ?? "")
                return
            }
            
            print(String.init(data: data!, encoding:String.Encoding.utf8) as Any)
            // make sure we got data
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            // parse the result as JSON, since that's what the API provides
            do {
                guard let todo = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String: AnyObject] else {
                    print("error trying to convert data to JSON")
                    return
                }
                let status : Bool = todo["status"] as! Bool
                print(status)
                if status == true{
                    let arrBusiness = NSMutableArray.init()
                    for dict in todo["data"] as! NSArray{
                        let objCat : BusinessList = BusinessList.init((dict as AnyObject as! NSDictionary) )
                        arrBusiness.add(objCat)
                    }
                    completionHandler(true,arrBusiness as NSMutableArray)
                }else{
                    completionHandler(false,NSMutableArray.init())
                }
                /*
                let arrBusiness = NSMutableArray.init()
                
                for dict in todo["data"] as! NSArray{
                    let objBusiness : BusinessList = BusinessList.init((dict as AnyObject as! NSDictionary) )
                    arrBusiness.add(objBusiness)
                }
                
                completionHandler(true,arrBusiness as NSMutableArray)*/
                
            } catch  {
                print("error trying to convert data to JSON")
                completionHandler(false,NSMutableArray.init())
                return
            }
            
        }
        
        task.resume()
    }
    
    // Get Event Category List
    func getEventCategoryList(completionHandler:@escaping (_ success :Bool,_ data: NSMutableArray?) -> Void){
        // Set up the URL request
        let todoEndpoint: String = "http://fameitc.com/outinthecity/api/v1/event/categories"
        guard let url = URL(string: todoEndpoint) else {
            print("Error: cannot create URL")
            return
        }
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "GET"
        urlRequest.setValue("Bearer " + User.sharedInstance.userToken, forHTTPHeaderField: "Authorization")
        
        // set up the session
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        // make the request
        let task = session.dataTask(with: urlRequest) {
            (data, response, error) in
            
            guard error == nil else {
                print("error calling GET on /todos/1")
                print(error ?? "")
                return
            }
            
            print(String.init(data: data!, encoding:String.Encoding.utf8) as Any)
            // make sure we got data
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            // parse the result as JSON, since that's what the API provides
            do {
                guard let todo = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String: AnyObject] else {
                    print("error trying to convert data to JSON")
                    return
                }
                print(todo)
                
                let status : Bool = todo["status"] as! Bool
                print(status)
                if status == true{
                    let arrEvent = NSMutableArray.init()
                    for dict in todo["data"] as! NSArray{
                        let objEvent : EventCategory = EventCategory.init((dict as AnyObject as! NSDictionary) )
                        arrEvent.add(objEvent)
                    }
                    completionHandler(true,arrEvent as NSMutableArray)
                }else{
                    completionHandler(false,NSMutableArray.init())
                }
                
            } catch  {
                print("error trying to convert data to JSON")
                completionHandler(false,NSMutableArray.init())
                return
            }
            
        }
        
        task.resume()
    }
    func getEventList(strEventCatId:String,completionHandler:@escaping (_ success :Bool,_ data: NSMutableArray?) -> Void){
        // Set up the URL request
        let todoEndpoint: String = "http://fameitc.com/outinthecity/api/v1/category/events"
        guard let url = URL(string: todoEndpoint) else {
            print("Error: cannot create URL")
            return
        }
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "POST"
        let paramString = String(format:"category_id=%@",strEventCatId)
        urlRequest.httpBody = paramString.data(using: String.Encoding.utf8)
        urlRequest.setValue("Bearer " + User.sharedInstance.userToken, forHTTPHeaderField: "Authorization")
        
        // set up the session
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        // make the request
        let task = session.dataTask(with: urlRequest) {
            (data, response, error) in
            
            guard error == nil else {
                print("error calling GET on /todos/1")
                print(error ?? "")
                return
            }
            
            print(String.init(data: data!, encoding:String.Encoding.utf8) as Any)
            // make sure we got data
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            // parse the result as JSON, since that's what the API provides
            do {
                guard let todo = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String: AnyObject] else {
                    print("error trying to convert data to JSON")
                    return
                }
                print(todo)
                
                let status : Bool = todo["status"] as! Bool
                print(status)
                if status == true{
                    let arrEvent = NSMutableArray.init()
                    for dict in todo["data"] as! NSArray{
                        let objEvent : Event = Event.init((dict as AnyObject as! NSDictionary) )
                        arrEvent.add(objEvent)
                    }
                    completionHandler(true,arrEvent as NSMutableArray)
                }else{
                    completionHandler(false,NSMutableArray.init())
                }
                
            } catch  {
                print("error trying to convert data to JSON")
                completionHandler(false,NSMutableArray.init())
                return
            }
            
        }
        
        task.resume()
    }
    
    // Get Event Category List
    func getLinkList(completionHandler:@escaping (_ success :Bool,_ data: NSMutableArray?) -> Void){
        // Set up the URL request
        let todoEndpoint: String = "http://fameitc.com/outinthecity/api/v1/events/linked/get"
        guard let url = URL(string: todoEndpoint) else {
            print("Error: cannot create URL")
            return
        }
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "GET"
        urlRequest.setValue("Bearer " + User.sharedInstance.userToken, forHTTPHeaderField: "Authorization")
        
        // set up the session
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        // make the request
        let task = session.dataTask(with: urlRequest) {
            (data, response, error) in
            
            guard error == nil else {
                print("error calling GET on /todos/1")
                print(error ?? "")
                return
            }
            
            print(String.init(data: data!, encoding:String.Encoding.utf8) as Any)
            // make sure we got data
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            // parse the result as JSON, since that's what the API provides
            do {
                guard let todo = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String: AnyObject] else {
                    print("error trying to convert data to JSON")
                    return
                }
                print(todo)
                
                let status : Bool = todo["status"] as! Bool
                print(status)
                if status == true{
                    let arrEvent = NSMutableArray.init()
                    for dict in todo["data"] as! NSArray{
                        let objEvent : Event = Event.init((dict as AnyObject as! NSDictionary))
                        arrEvent.add(objEvent)
                    }
                    completionHandler(true,arrEvent as NSMutableArray)
                }else{
                    completionHandler(false,NSMutableArray.init())
                }
                
            } catch  {
                print("error trying to convert data to JSON")
                completionHandler(false,NSMutableArray.init())
                return
            }
        }
        
        task.resume()
    }
    
    // Going Button Action
    func goingToButtonAction(strEventId:String,completionHandler:@escaping (_ success :Bool,_ data: String?) -> Void){
        // Set up the URL request
        let todoEndpoint: String = "http://fameitc.com/outinthecity/api/v1/event/link"
        guard let url = URL(string: todoEndpoint) else {
            print("Error: cannot create URL")
            return
        }
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "POST"
        let paramString = String(format:"event_id=%@",strEventId)
        urlRequest.httpBody = paramString.data(using: String.Encoding.utf8)
        urlRequest.setValue("Bearer " + User.sharedInstance.userToken, forHTTPHeaderField: "Authorization")
        
        // set up the session
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        // make the request
        let task = session.dataTask(with: urlRequest) {
            (data, response, error) in
            
            guard error == nil else {
                print("error calling GET on /todos/1")
                print(error ?? "")
                return
            }
            
            print(String.init(data: data!, encoding:String.Encoding.utf8) as Any)
            // make sure we got data
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            // parse the result as JSON, since that's what the API provides
            do {
                guard let todo = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String: AnyObject] else {
                    print("error trying to convert data to JSON")
                    return
                }
                print(todo)
                
                let status : Bool = todo["status"] as! Bool
                print(status)
                if status == true{
                    let messageButton : String = todo["message"] as! String
                    completionHandler(true,messageButton as String)
                }else{
                    completionHandler(false,String.init())
                }
                
            } catch  {
                print("error trying to convert data to JSON")
                completionHandler(false,String.init())
                return
            }
            
        }
        
        task.resume()
    }
    
    // Get Navigation List
    func getNavigationList(completionHandler:@escaping (_ success :Bool,_ data: NSMutableArray?) -> Void){
        // Set up the URL request
        let todoEndpoint: String = "http://fameitc.com/outinthecity/api/v1/establishments/all"
        guard let url = URL(string: todoEndpoint) else {
            print("Error: cannot create URL")
            return
        }
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "GET"
        urlRequest.setValue("Bearer " + User.sharedInstance.userToken, forHTTPHeaderField: "Authorization")
        
        // set up the session
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        // make the request
        let task = session.dataTask(with: urlRequest) {
            (data, response, error) in
            
            guard error == nil else {
                print("error calling GET on /todos/1")
                print(error ?? "")
                return
            }
            
            print(String.init(data: data!, encoding:String.Encoding.utf8) as Any)
            // make sure we got data
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            // parse the result as JSON, since that's what the API provides
            do {
                guard let todo = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String: AnyObject] else {
                    print("error trying to convert data to JSON")
                    return
                }
                print(todo)
                
                let status : Bool = todo["status"] as! Bool
                print(status)
                if status == true{
                    let arrBusiness = NSMutableArray.init()
                    for dict in todo["data"] as! NSArray{
                        let objCat : BusinessList = BusinessList.init((dict as AnyObject as! NSDictionary) )
                        arrBusiness.add(objCat)
                    }
                    completionHandler(true,arrBusiness as NSMutableArray)
                }else{
                    completionHandler(false,NSMutableArray.init())
                }
                
            } catch  {
                print("error trying to convert data to JSON")
                completionHandler(false,NSMutableArray.init())
                return
            }
        }
        
        task.resume()
    }
}
