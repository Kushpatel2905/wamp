//
//  PlaceMapViewController.swift
//  Wamp
//
//  Created by Kushal Patel on 10/13/17.
//  Copyright © 2017 Kushal Patel. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
class PlaceMapViewController: UIViewController {

    fileprivate var places = [Place]()
    fileprivate let locationManager = CLLocationManager()
    @IBOutlet weak var mapView: MKMapView!
    var arViewController: ARViewController!
    var startedLoadingPOIs = false
    fileprivate var Businessplaces = [Place]()
    var arrBusiness : NSMutableArray = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Map"
       // locationManager.delegate = self
       // locationManager.desiredAccuracy = kCLLocationAccuracyBest
       // locationManager.startUpdatingLocation()
       // locationManager.requestWhenInUseAuthorization()
       // mapView.userTrackingMode = MKUserTrackingMode.followWithHeading

        self.navigationController?.isNavigationBarHidden = false

        let settingsButton = UIBarButtonItem(title: NSString(string: "Camera") as String, style: .plain, target: self, action: #selector(PlaceMapViewController.showARController))
        let font = UIFont.systemFont(ofSize: 14) // adjust the size as required
        let attributes = [NSFontAttributeName : font]
        settingsButton.setTitleTextAttributes(attributes, for: .normal)
        
        self.navigationItem.rightBarButtonItem = settingsButton
        
        getAllEshtablishment()
        // Do any additional setup after loading the view.
    }
    
    func getAllEshtablishment() -> Void {
        let objDataMange = DataManager.init()
        SwiftLoader.show(animated: true)
        objDataMange.getNavigationList{ (issuccess, arr) in
            DispatchQueue.main.sync {
                SwiftLoader.hide()
                let arrBusiness : NSMutableArray! = NSMutableArray.init(array: arr!)
                self.arrBusiness = NSMutableArray.init(array: arr!)
                
                for i in (0..<(arrBusiness as AnyObject).count)
                {
                    let objBusiness = arrBusiness.object(at: i) as! BusinessList
                    let latitude = Double(objBusiness.businessLatitude)!
                    let longitude = Double(objBusiness.businessLongitude)!
                    let location = CLLocation(latitude: latitude, longitude: longitude)
                    let place = Place(location: location, reference: objBusiness.businessFirstname, name: objBusiness.businessBusinessName, address: objBusiness.businessAddress)
                    self.Businessplaces.append(place)
                    self.places.append(place)
                    let annotation = PlaceAnnotation(location: place.location!.coordinate, title: place.placeName)
                    DispatchQueue.main.async {
                        self.mapView.addAnnotation(annotation)
                    }
                }
                DispatchQueue.main.async {
                    self.mapView.showAnnotations(self.mapView.annotations, animated: true)
                }
                
                
            }
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func goBaCK(_ sender: Any) {
    self.navigationController?.popViewController(animated: true)
    }
    
   @IBAction func showARController(_ sender: Any) {
    
    if self.Businessplaces.count > 0{
        self.arViewController = ARViewController()
        self.arViewController.dataSource = self
        self.arViewController.maxDistance = 0
        self.arViewController.maxVisibleAnnotations = 30
        self.arViewController.maxVerticalLevel = 5
        self.arViewController.headingSmoothingFactor = 0.05
        self.arViewController.trackingManager.userDistanceFilter = 25
        self.arViewController.trackingManager.reloadDistanceFilter = 75
        self.arViewController.setAnnotations(self.Businessplaces)
        self.arViewController.uiOptions.debugEnabled = false
        self.arViewController.uiOptions.closeButtonEnabled = true
        self.arViewController.navigationController?.isNavigationBarHidden = true
        
        self.arViewController.isSubCategory = true
        self.arViewController.arrBusiness = self.arrBusiness
        let navController = UINavigationController(rootViewController: self.arViewController)
        self.present(navController, animated: true, completion: nil)
    }
    
   /* arViewController = ARViewController()
    arViewController.dataSource = self
    arViewController.maxDistance = 0
    arViewController.maxVisibleAnnotations = 30
    arViewController.maxVerticalLevel = 5
    arViewController.headingSmoothingFactor = 0.05
    
    arViewController.trackingManager.userDistanceFilter = 25
    arViewController.trackingManager.reloadDistanceFilter = 75
    arViewController.setAnnotations(places)
    arViewController.uiOptions.debugEnabled = false
    arViewController.uiOptions.closeButtonEnabled = true
    
    self.present(arViewController, animated: true, completion: nil)*/
  }
  
  func showInfoView(forPlace place: Place) {
    let alert = UIAlertController(title: place.placeName , message: place.infoText, preferredStyle: UIAlertControllerStyle.alert)
    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
    
    arViewController.present(alert, animated: true, completion: nil)
  }
}

extension PlaceMapViewController: CLLocationManagerDelegate {
  func locationManagerShouldDisplayHeadingCalibration(_ manager: CLLocationManager) -> Bool {
    return true
  }
  
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    
    if locations.count > 0 {
      let location = locations.last!
      if location.horizontalAccuracy < 100 {
        manager.stopUpdatingLocation()
        //let span = MKCoordinateSpan(latitudeDelta: 0.014, longitudeDelta: 0.014)
        //let region = MKCoordinateRegion(center: location.coordinate, span: span)
       // mapView.region = region
        
        if !startedLoadingPOIs {
          startedLoadingPOIs = true
          let loader = PlacesLoader()
          loader.loadPOIS(location: location, radius: 1000) { placesDict, error in
            if let dict = placesDict {
              guard let placesArray = dict.object(forKey: "results") as? [NSDictionary]  else { return }
              
              for placeDict in placesArray {
                let latitude = placeDict.value(forKeyPath: "geometry.location.lat") as! CLLocationDegrees
                let longitude = placeDict.value(forKeyPath: "geometry.location.lng") as! CLLocationDegrees
                let reference = placeDict.object(forKey: "reference") as! String
                let name = placeDict.object(forKey: "name") as! String
                let address = placeDict.object(forKey: "vicinity") as! String
                
                let location = CLLocation(latitude: latitude, longitude: longitude)
                let place = Place(location: location, reference: reference, name: name, address: address)
                
               // self.places.append(place)
                let annotation = PlaceAnnotation(location: place.location!.coordinate, title: place.placeName)
                DispatchQueue.main.async {
                 // self.mapView.addAnnotation(annotation)
                }
              }
                
                
            }
          }
        }
      }
    }
  }
}

extension PlaceMapViewController: ARDataSource {
  func ar(_ arViewController: ARViewController, viewForAnnotation: ARAnnotation) -> ARAnnotationView {
    let annotationView = AnnotationView()
    annotationView.annotation = viewForAnnotation
    annotationView.delegate = self
    annotationView.frame = CGRect(x: 0, y: 0, width: 150, height: 50)
    
    return annotationView
  }
}

extension PlaceMapViewController: AnnotationViewDelegate {
  func didTouch(annotationView: AnnotationView) {
    if let annotation = annotationView.annotation as? Place {
      let placesLoader = PlacesLoader()
      placesLoader.loadDetailInformation(forPlace: annotation) { resultDict, error in
        
        if let infoDict = resultDict?.object(forKey: "result") as? NSDictionary {
          annotation.phoneNumber = infoDict.object(forKey: "formatted_phone_number") as? String
          annotation.website = infoDict.object(forKey: "website") as? String
          
          self.showInfoView(forPlace: annotation)
        }
      }
      
    }
  }
}
